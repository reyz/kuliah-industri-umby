<?php

use App\Entities;

$bindToEntityOr404 = function ($class, $findKey = 'id') {
    return function ($value) use ($class, $findKey) {
        $entity = EntityManager::getRepository($class)
            ->findOneBy([$findKey => $value]);

        if (intval($value) === 0) {
            $value = str_replace("-", " ", $value);
            $entity = EntityManager::getRepository($class)
                ->findOneBy([$findKey => $value]);
        }

        if ($entity instanceof $class) {
            return $entity;
        }

        return abort(404);
    };
};

Route::bind('page', $bindToEntityOr404(Entities\Pages::class, 'id'));
Route::bind('story', $bindToEntityOr404(Entities\Stories::class, 'id'));
Route::bind('plot', $bindToEntityOr404(Entities\Plots::class, 'id'));
Route::bind('song', $bindToEntityOr404(Entities\Songs::class, 'id'));
Route::bind('title', $bindToEntityOr404(Entities\Songs::class, 'id'));
Route::bind('game', $bindToEntityOr404(Entities\Games::class, 'id'));
Route::pattern('id', '[0-9]+');
