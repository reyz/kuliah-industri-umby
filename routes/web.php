<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require_once 'bind.php';

Route::prefix('/administrator')->group(function () {
    Route::group(['middleware' => ['admin_auth']], function () {
        Route::get('/', function () {
            return redirect(route('back.page-index'));
        });

        Route::prefix('/page')->group(function () {
            Route::any('/add', 'Back\PageController@add')
                ->name('back.page-add');
            Route::any('/{page}/view', 'Back\PageController@view')
                ->name('back.page-view');
            Route::any('/{page}/edit', 'Back\PageController@edit')
                ->name('back.page-edit');
            Route::get('/', 'Back\PageController@index')
                ->name('back.page-index');
        });

        Route::prefix('/game')->group(function () {
            Route::any('/add', 'Back\GameController@add')
                ->name('back.game-add');
            Route::any('/{game}/edit', 'Back\GameController@edit')
                ->name('back.game-edit');
            Route::get('/{game}/delete', 'Back\GameController@delete')
                ->name('back.game-delete');
            Route::get('/{game}/view', 'Back\GameController@view')
                ->name('back.game-view');
            Route::get('/', 'Back\GameController@index')
                ->name('back.game-index');
        });

        Route::prefix('/song')->group(function () {
            Route::any('/add', 'Back\SongController@add')
                ->name('back.song-add');
            Route::any('/{song}/edit', 'Back\SongController@edit')
                ->name('back.song-edit');
            Route::get('/{song}/delete', 'Back\SongController@delete')
                ->name('back.song-delete');
            Route::get('/{song}/view', 'Back\SongController@view')
                ->name('back.song-view');
            Route::get('/', 'Back\SongController@index')
                ->name('back.song-index');
        });

        Route::prefix('/story')->group(function () {
            Route::any('/add', 'Back\StoryController@add')
                ->name('back.story-add');
            Route::any('/{story}/edit', 'Back\StoryController@edit')
                ->name('back.story-edit');
            Route::get('/{story}/delete', 'Back\StoryController@delete')
                ->name('back.story-delete');
            Route::get('/{story}/view', 'Back\StoryController@view')
                ->name('back.story-view');
            Route::get('/', 'Back\StoryController@index')
                ->name('back.story-index');

            Route::prefix('/{story}/plot')->group(function () {
                Route::any('/add', 'Back\PlotController@add')
                    ->name('back.plot-add');
                Route::any('/{plot}/edit', 'Back\PlotController@edit')
                    ->name('back.plot-edit');
                Route::get('/{plot}/delete', 'Back\PlotController@delete')
                    ->name('back.plot-delete');
                Route::post('/{plot}/updatePage',
                    'Back\PlotController@updatePagePlots')
                    ->name('back.plot-update-page');
                Route::get('/', 'Back\PlotController@index')
                    ->name('back.plot-index');
            });
        });

        Route::prefix('/ranking')->group(function () {
            Route::any('/', 'Back\RankingController@index')
                ->name('back.ranking-index');
            Route::any('/allLogs', 'Back\RankingController@rankingData')
                ->name('back.ranking-data');
        });
    });

    Route::get('/logout', 'Back\AuthController@logout')->name('back.logout');
    Route::any('/login', 'Back\AuthController@login')->name('back.login');
});

Route::prefix('/home')->group(function () {
    Route::prefix('/song')->group(function () {
        Route::get('/', 'Front\SongController@index')
            ->name('front.song-index');
        Route::prefix('/list')->group(function () {
            Route::get('/', 'Front\SongController@list')
                ->name('front.song-list');
            Route::get('/{song}/view',
                'Front\SongController@view')
                ->name('front.song-view');
        });
    });

    Route::prefix('/story')->group(function () {
        Route::get('/', 'Front\StoryController@index')
            ->name('front.story-index');
        Route::prefix('/list')->group(function () {
            Route::get('/', 'Front\StoryController@list')
                ->name('front.story-list');
            Route::get('/{story}/view/{story_page?}',
                'Front\StoryController@view')
                ->name('front.story-view');
        });
    });

    Route::prefix('/game')->group(function () {
        Route::get('/', 'Front\GameController@index')
            ->name('front.game-index');
        Route::prefix('/list')->group(function () {
            Route::get('/', 'Front\GameController@list')
                ->name('front.game-list');
            Route::get('/{game}/view',
                'Front\GameController@view')
                ->name('front.game-view');
        });
    });

    Route::get('/{halaman}', 'Front\PageController@view')
        ->name('front.page-view');

    Route::get('/', 'Front\PageController@index')->name('front.home-index');
});

Route::get('/', function () {
    return redirect(route('front.home-index'));
});
