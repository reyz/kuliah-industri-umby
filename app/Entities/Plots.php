<?php

namespace App\Entities;

use DateTime;
use Doctrine\ORM\Mapping as ORM;


/**
 * Plots
 *
 * @ORM\Table(name="plots", indexes={@ORM\Index(name="story_id_stories", columns={"story_id"})})
 * @ORM\Entity
 */
class Plots
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false,
     *                        options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="plot_title", type="string", length=255, nullable=false)
     */
    private $plotTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=true)
     */
    private $content;

    /**
     * @var int
     *
     * @ORM\Column(name="page", type="integer", nullable=false)
     */
    private $page;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var Stories
     *
     * @ORM\ManyToOne(targetEntity="Stories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="story_id", referencedColumnName="id")
     * })
     */
    private $story;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPlotTitle(): string
    {
        return $this->plotTitle;
    }

    /**
     * @param string $plotTitle
     */
    public function setPlotTitle(string $plotTitle): void
    {
        $this->plotTitle = $plotTitle;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function isPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime|null $updatedAt
     */
    public function setUpdatedAt(?DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return Stories
     */
    public function getStory(): Stories
    {
        return $this->story;
    }

    /**
     * @param Stories $story
     */
    public function setStory(Stories $story): void
    {
        $this->story = $story;
    }
}
