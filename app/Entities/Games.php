<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permainan
 *
 * @ORM\Table(name="games")
 * @ORM\Entity
 */
class Games
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false,
     *                        options={"unsigned"=true})
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="history", type="text", length=65535, nullable=true)
     */
    private $history;

    /**
     * @var string
     *
     * @ORM\Column(name="philosophy", type="text", length=65535, nullable=false)
     */
    private $philosophy;

    /**
     * @var string
     *
     * @ORM\Column(name="how_to_play", type="text", length=65535,
     *                                 nullable=false)
     */
    private $how_to_play;

    /**
     * @var string
     *
     * @ORM\Column(name="picture", type="string", length=255, nullable=false)
     */
    private $picture;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * @param string $history
     */
    public function setHistory(string $history): void
    {
        $this->history = $history;
    }

    /**
     * @return string
     */
    public function getPhilosophy(): string
    {
        return $this->philosophy;
    }

    /**
     * @param string $philosophy
     */
    public function setPhilosophy(string $philosophy): void
    {
        $this->philosophy = $philosophy;
    }

    /**
     * @return string
     */
    public function getHowToPlay(): string
    {
        return $this->how_to_play;
    }

    /**
     * @param string $caraMain
     */
    public function setHowToPlay(string $caraMain): void
    {
        $this->how_to_play = $caraMain;
    }

    /**
     * @return string
     */
    public function getPicture(): string
    {
        return $this->picture;
    }

    /**
     * @param string $picture
     */
    public function setPicture(string $picture): void
    {
        $this->picture = $picture;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }
}
