<?php

namespace App\Entities;

use DateTime;
use Doctrine\ORM\Mapping as ORM;


/**
 * PlotAssets
 *
 * @ORM\Table(name="plot_assets", indexes={@ORM\Index(name="plot_id_plots", columns={"plot_id"})})
 * @ORM\Entity
 */
class PlotAssets
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=false)
     */
    private $filename;

    /**
     * @var Plots
     *
     * @ORM\ManyToOne(targetEntity="Plots")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plot_id", referencedColumnName="id")
     * })
     */
    private $plot;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename(string $filename): void
    {
        $this->filename = $filename;
    }

    /**
     * @return Plots
     */
    public function getPlot(): Plots
    {
        return $this->plot;
    }

    /**
     * @param Plots $plot
     */
    public function setPlot(Plots $plot): void
    {
        $this->plot = $plot;
    }
}
