<?php

namespace App\Entities;

interface UserInterface
{
    public function getUsername();
    public function getPassword();
}
