<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * ViewLogs
 *
 * @ORM\Table(name="view_logs")
 * @ORM\Entity
 */
class ViewLogs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false,
     *                        options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="category_text", type="string", length=255,
     *                                   nullable=false)
     */
    private $categoryText;

    /**
     * @var string
     *
     * @ORM\Column(name="content_id", type="integer", length=255,
     *                                   nullable=false)
     */
    private $contentId;

    /**
     * @var string
     *
     * @ORM\Column(name="created_at", type="string", length=50, nullable=false)
     */
    private $createdAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCategoryText(): string
    {
        return $this->categoryText;
    }

    /**
     * @param string $categoryText
     */
    public function setCategoryText(string $categoryText): void
    {
        $this->categoryText = $categoryText;
    }

    /**
     * @return int
     */
    public function getContentId(): int
    {
        return $this->contentId;
    }

    /**
     * @param int $contentId
     */
    public function setContentId(int $contentId): void
    {
        $this->contentId = $contentId;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }


}
