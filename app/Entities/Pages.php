<?php

namespace App\Entities;

use DateTime;
use Doctrine\ORM\Mapping as ORM;


/**
 * Pages
 *
 * @ORM\Table(name="pages")
 * @ORM\Entity
 */
class Pages
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false,
     *                        options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=false)
     */
    private $content;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false,
     *                                options={"default"="CURRENT_TIMESTAMP"})
     */
//    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
//    private $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return DateTime
     */
//    public function getCreatedAt(): DateTime
//    {
//        return $this->createdAt;
//    }

    /**
     * @param DateTime $createdAt
     */
//    public function setCreatedAt(DateTime $createdAt): void
//    {
//        $this->createdAt = $createdAt;
//    }

    /**
     * @return DateTime|null
     */
//    public function getUpdatedAt(): ?DateTime
//    {
//        return $this->updatedAt;
//    }

    /**
     * @param DateTime|null $updatedAt
     */
//    public function setUpdatedAt(?DateTime $updatedAt): void
//    {
//        $this->updatedAt = $updatedAt;
//    }
}
