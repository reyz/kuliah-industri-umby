<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Song
 *
 * @ORM\Table(name="songs")
 * @ORM\Entity
 */
class Songs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0,
     *                        nullable=false, options={"unsigned"=true},
     *                        unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50, precision=0,
     *                           scale=0, nullable=false, unique=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="video", type="string", length=255, precision=0,
     *                           scale=0, nullable=false, unique=false)
     */
    private $video;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, precision=0,
     *                                 scale=0, nullable=true, unique=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=10, precision=0, scale=0,
     *                          nullable=true, unique=false)
     */
    private $type;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Songs
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get video.
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set video.
     *
     * @param string $video
     *
     * @return Songs
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Songs
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Songs
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
}
