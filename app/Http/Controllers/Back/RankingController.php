<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Services\Application\RankingService;
use Illuminate\Http\Request;

class RankingController extends Controller
{
    public function index()
    {
        return view('back.ranking.view');
    }

    /**
     * @param RankingService $rankingService
     * @param Request $request
     */
    public function rankingData(
        RankingService $rankingService,
        Request $request
    ) {
        $isSearchDate = $request->input('isDateSearch');
        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');

        $logsData = $rankingService->getLogsData($isSearchDate, $startDate, $endDate);

        if ($startDate) {
            $start = new \DateTime($startDate);
            $end = new \DateTime($endDate);
        }

        $json_data = [
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($logsData['total']),
            "recordsFiltered" => intval($logsData['total']),
            "data"            => $logsData['data'],
            "startDate"       => $startDate ? $start->format('Y-m-d') : null,
            "endDate"         => $endDate ? $end->format('Y-m-d') : null
        ];

        echo json_encode($json_data);
    }
}
