<?php

namespace App\Http\Controllers\Back;

use App\Entities\Pages;
use App\Http\Controllers\Controller;
use App\Services\Domain\PageService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class PageController extends Controller
{
    public function index(PageService $pageService)
    {
        $pages = $pageService->getAllPage();

        return view('back.page.index', compact('pages'));
    }

    public function add(
        Request $request,
        PageService $pageService
    )
    {
        if ($request->method() === 'POST') {
            try {
                $this->validate($request, [
                    'title' => 'required',
                    'description' => 'required'
                ]);

                $pageService->create(collect($request->input()));

                $notification = 'Halaman baru berhasil dibuat';

                return redirect()->route('back.page-index')
                    ->with('system_notification', $notification);
            } catch (ValidationException $e) {
                return redirect()->back()->withInput()
                    ->withErrors($e->errors());
            } catch (Exception $e) {
                $notification
                    = 'Terjadi Kesalahan, silahkan hubungi web administrator';
                return redirect()->route('back.page-index')
                    ->with('error_notification', $notification);
            }
        }
        return view('back.page.create');
    }

    public function view(
        Pages $page
    )
    {
        return view('back.page.view', compact('page'));
    }

    /**
     * @param Pages $page
     * @param Request $request
     * @param PageService $pageService
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(
        Pages $page,
        Request $request,
        PageService $pageService
    )
    {
        if (request()->method() == 'POST') {

            $roles = [
                'title' => 'required',
                'description' => 'required'
            ];

            try {
                $this->validate($request, $roles);
                $pageService->update($page, collect($request->input()));
                $message = 'Halaman berhasil diubah!';

                return redirect()->route('back.page-index')
                    ->with('system_notification', $message);
            } catch (ValidationException $e) {
                return redirect()->back()->withInput()
                    ->withErrors($e->errors());
            } catch (Exception $e) {
                $message
                    = 'Cannot process request. Please contact web administrator!';

                return redirect()->route('back.page-index')
                    ->with('system_notification', $message);
            }
        }

        return view('back.page.edit', compact('page'));
    }
}
