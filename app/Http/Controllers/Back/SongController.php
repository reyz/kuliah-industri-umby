<?php

namespace App\Http\Controllers\Back;

use App\Entities\Songs;
use App\Http\Controllers\Controller;
use App\Services\Domain\SongService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

/**
 * Class SongController
 *
 * @package App\Http\Controllers\Back
 */
class SongController extends Controller
{
    /**
     * @param SongService $songService
     *
     * @return Factory|View
     */
    public function index(SongService $songService)
    {
        $songs = $songService->getAllSong();

        return view('back.song.index', compact('songs'));
    }

    /**
     * @param Request $request
     * @param SongService $songService
     *
     * @return Factory|RedirectResponse|View
     */
    public function add(Request $request, SongService $songService)
    {
        if ($request->method() === 'POST') {
            try {
                $this->validate($request, [
                    'title' => 'required',
                    'description' => 'required',
                    'video' => 'required'
                ]);

                $video = $songService->upload($request, 'video');
                $songService->create(collect($request->input()), $video);

                $notification = 'Berhasil menambahkan lagu baru';
                return redirect()->route('back.song-index')
                    ->with('notification', $notification);
            } catch (ValidationException $e) {
                return redirect()->back()->withInput()
                    ->withErrors($e->errors());
            } catch (Exception $ex) {
                $notification
                    = 'Terjadi kesalahan, silahkan hubungi web administrator' . $ex->getMessage();

                return redirect()->route('back.song-index')
                    ->with('notification', $notification);
            }
        }

        return view('back.song.create');
    }

    /**
     * @param Songs $songs
     * @param Request $request
     * @param SongService $songService
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(
        Songs $songs,
        Request $request,
        SongService $songService
    )
    {
        if ($request->method() === 'POST') {
            try {
                $video = null;
                $this->validate($request, [
                    'title' => 'required',
                    'description' => 'required'
                ]);

                if (!is_null($request->file('video'))) {
                    $video = $songService->upload($request, 'video');
                }

                $songService->update(
                    $songs,
                    collect($request->input()),
                    $video
                );

                $notification = 'Berhasil memperbaharui Lagu';
                return redirect()->route('back.song-index')
                    ->with('notification', $notification);
            } catch (ValidationException $e) {
                return redirect()->back()->withInput()
                    ->withErrors($e->errors());
            } catch (Exception $e) {
                $notification
                    = 'Terjadi kesalahan, silahkan hubungi web administrator';
                return redirect()->route('back.song-index')
                    ->with('error_notification', $notification);
            }
        }

        return view('back.song.edit', compact('songs'));
    }

    /**
     * @param Songs $songs
     * @param SongService $songService
     *
     * @return RedirectResponse
     */
    public function delete(Songs $songs, SongService $songService)
    {
        try {
            $songService->delete($songs);
            $notification = 'Berhasil menghapus Lagu';

            return redirect()->route('back.song-index')
                ->with('notification', $notification);
        } catch (Exception $e) {
            $notification
                = 'Terjadi kesalahan, silahkan hubungi web administrator';

            return redirect()->route('back.song-index')
                ->with('error_notification', $notification);
        }
    }

    /**
     * @param int $id
     * @param SongService $songService
     *
     * @return false|string
     */
    public function view(int $id, SongService $songService)
    {
        $song = $songService->findSongById($id);
        $song = [
            'title' => $song->getTitle(),
            'description' => $song->getDescription(),
            'type' => $song->getType(),
            'video' => $song->getVideo()
        ];

        /** @noinspection PhpComposerExtensionStubsInspection */
        return json_encode($song);
    }
}
