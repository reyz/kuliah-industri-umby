<?php

namespace App\Http\Controllers\Back;

use App\Entities\Stories;
use App\Http\Controllers\Controller;
use App\Services\Domain\StoryService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class StoryController extends Controller
{
    /**
     * @param StoryService $storyService
     *
     * @return Factory|View
     */
    public function index(StoryService $storyService)
    {
        $stories = $storyService->getRepository()->findAll();

        return view('back.story.story', compact('stories'));
    }

    /**
     * @param Request $request
     * @param StoryService $storyService
     *
     * @return Factory|RedirectResponse|View
     */
    public function add(Request $request, StoryService $storyService)
    {
        if ($request->method() === 'POST') {
            try {
                $this->validate($request, [
                    'title' => 'required|min:3',
                ]);

                try {
                    $notification
                        = 'Berhasil menambahkan Story';
                    $storyService->create(collect($request->input()));

                    return redirect()->route('back.story-index')
                        ->with('notification', $notification);
                } catch (Exception $ex) {
                    $notification
                        = 'Terjadi kesalahan, silahkan hubungi web administrator';

                    return redirect()->route('back.story-index')
                        ->with('error_notification', $notification);
                }

            } catch (ValidationException $e) {
                return redirect()->back()->withInput()
                    ->withErrors($e->errors());
            }
        }
        return view('back.story.create');
    }

    /**
     * @param Request $request
     * @param StoryService $storyService
     * @param Stories $story
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(
        Request $request,
        StoryService $storyService,
        Stories $story
    )
    {
        if ($request->method() === 'POST') {
            try {
                $this->validate($request, [
                    'title' => 'required|min:3',
                ]);

                try {
                    $storyService->update($story, collect($request->input()));

                    $notification = 'Berhasil melakukan perubahan pada Story';
                    return redirect()->route('back.story-index')
                        ->with('notification', $notification);
                } catch (Exception $ex) {
                    $notification
                        = 'Terjadi kesalahan, silahkan hubungi web administrator';
                    return redirect()->route('back.story-index')
                        ->with('error_notification', $notification);
                }
            } catch (ValidationException $e) {
                return redirect()->back()->withInput()
                    ->withErrors($e->errors());
            }
        }
        return view('back.story.edit', compact('story'));
    }

    /**
     * @param Stories $stories
     * @param StoryService $storyService
     *
     * @return RedirectResponse
     */
    public function delete(Stories $stories, StoryService $storyService)
    {
        try {
            $storyService->delete($stories);

            $notification
                = 'Berhasil menghapus cerita';
            return redirect()->route('back.story-index')
                ->with('notification', $notification);
        } catch (Exception $ex) {
            $notification
                = 'Terjadi kesalahan, silahkan hubungi web administrator';

            return redirect()->route('back.story-index')
                ->with('error_notification', $notification);
        }
    }

    /**
     * @param Stories $story
     * @param StoryService $storyService
     *
     * @return Factory|View
     */
    public function view(Stories $story, StoryService $storyService)
    {
        $stories = $storyService->getPlotByStory($story);
        return view('back.story.view', compact('stories', 'story'));
    }
}
