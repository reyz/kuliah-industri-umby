<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Services\Application\AuthService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request, AuthService $authService)
    {
        if (request()->method() == 'POST') {
            $request->validate([
                'username' => 'required',
                'password' => 'required|min:8|max:16'
            ]);

            if ($authService->auth(request('username'), request('password'))) {
                return redirect(session('authenticated')['type']);
            }

            $request->session()->flash('message', 'Cannot logged you in!');
        }

        return view('back.login');
    }

    public function logout(AuthService $authService)
    {
        $authService->logout();

        return redirect()->route('back.login');
    }
}
