<?php

namespace App\Http\Controllers\Back;

use App\Entities\Plots;
use App\Entities\Stories;
use App\Http\Controllers\Controller;
use App\Services\Domain\PlotService;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

/**
 * Class PlotController
 *
 * @package App\Http\Controllers\Back
 */
class PlotController extends Controller
{
    /**
     * @param PlotService $plotService
     * @param Stories $stories
     *
     * @return Factory|View
     */
    public function index(PlotService $plotService, Stories $stories)
    {
        $plots = $plotService->getRepository()
            ->findBy(['story' => $stories], ['page' => 'asc']);

        return view('back.story.plots.plots', compact('plots', 'stories'));
    }

    /**
     * @param Request $request
     * @param PlotService $plotService
     * @param Stories $stories
     *
     * @return Factory|RedirectResponse|View
     */
    public function add(
        Request $request,
        PlotService $plotService,
        Stories $stories
    )
    {

        if ($request->method() === 'POST') {
            try {
                $this->validate($request, [
                    'plot_title' => 'required|min:4',
                    'content' => 'required|min:4',
                    'picture' => 'required',
                ]);

                $picture = $plotService->upload($request, 'picture');

                $plotService->create(
                    $stories,
                    collect($request->input()),
                    $picture
                );

                $notification = 'Berhasil menambahkan Plots Cerita';
                return redirect()
                    ->route('back.plot-index', [$stories->getId()])
                    ->with('notification', $notification);
            } catch (ValidationException $e) {
                return redirect()->back()->withInput()
                    ->withErrors($e->errors());
            } catch (Exception $ex) {
                $notification
                    = 'Terjadi kesalahan, silahkan hubungi web administrator';

                return redirect()
                    ->route('back.plot-index', [$stories->getId()])
                    ->with('error_notification', $notification);
            }
        }

        $plots = $plotService->getRepository()->findAll();

        return view('back.story.plots.create', compact('plots', 'stories'));
    }

    /**
     * @param Request $request
     * @param Stories $stories
     * @param Plots $plots
     * @param PlotService $plotService
     *
     * @return Factory|RedirectResponse|View
     */
    public function edit(
        Request $request,
        Stories $stories,
        Plots $plots,
        PlotService $plotService
    )
    {
        if ($stories->getId() == $plots->getStory()->getId()) {
            if ($request->method() === 'POST') {
                try {
                    $this->validate($request, [
                        'plot_title' => 'required|min:4',
                        'content' => 'required|min:4',
                    ]);

                    $picture = false;
                    if ($request->file('picture')) {
                        $picture = $plotService->upload($request, 'picture');
                    }
                    $plotService->update(
                        $plots,
                        collect($request->input()),
                        $picture
                    );

                    $notification
                        = 'Berhasil melakukan perubahan pada Plots';
                    return redirect()
                        ->route('back.plot-index', $plots->getStory()->getId())
                        ->with('notification', $notification);
                } catch (ValidationException $e) {
                    redirect()->back()->withInput()->withErrors($e->errors());
                } catch (Exception $ex) {
                    $notification
                        = 'Terjadi kesalahan, silahkan hubungi web administrator';
                    print_r($ex->getMessage());

                    return redirect()
                        ->route('back.plot-index', $plots->getStory()->getId())
                        ->with('error_notification', $notification);
                }
            }

            $plots = $plotService->getPlotAndAsset($plots);

            return view('back.story.plots.edit', compact('plots'));
        }

        return abort(404);
    }

    /**
     * @param Stories $stories
     * @param Plots $plots
     * @param PlotService $plotService
     *
     * @return RedirectResponse
     */
    public function delete(
        Stories $stories,
        Plots $plots,
        PlotService $plotService
    )
    {
        try {
            $plotService->delete($stories, $plots);

            $notification = 'Berhasil menghapus Plots';
            return redirect()
                ->route('back.plot-index', $plots->getStory()->getId())
                ->with('notification', $notification);
        } catch (Exception $e) {
            $notification
                = 'Terjadi kesalahan, silahkan hubungi web administrator';

            return redirect()
                ->route('back.plot-index', $plots->getStory()->getId())
                ->with('error_notification', $notification);
        }
    }

    /**
     * @param Request $request
     * @param Stories $stories
     * @param Plots $plots
     * @param PlotService $plotService
     *
     * @throws OptimisticLockException
     */
    public function updatePagePlots(
        Request $request,
        Stories $stories,
        Plots $plots,
        PlotService $plotService
    )
    {
        $newPage = $request->input('newPage');

        $plotService->changePage($stories, $plots, $newPage);
    }
}
