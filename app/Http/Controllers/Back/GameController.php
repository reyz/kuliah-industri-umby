<?php

namespace App\Http\Controllers\Back;

use App\Entities\Games;
use App\Http\Controllers\Controller;
use App\Services\Domain\GameService;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

/**
 * Class GameController
 *
 * @package App\Http\Controllers\Back
 */
class GameController extends Controller
{
    /**
     * @param GameService $gameService
     *
     * @return Factory|View
     */
    public function index(GameService $gameService)
    {
        $games = $gameService->getAllGames();

        return view('back.games.index', compact('games'));
    }

    /**
     * @param Request     $request
     * @param GameService $gameService
     *
     * @return Factory|RedirectResponse|View
     */
    public function add(Request $request, GameService $gameService)
    {
        if ($request->method() === 'POST') {
            try {
                $this->validate($request, [
                    'title'       => 'required',
                    'history'     => 'required',
                    'philosophy'  => 'required',
                    'how-to-play' => 'required',
                    'image'       => 'required'
                ]);

                $picture = $gameService->upload($request, 'image', 'games');

                $gameService->create(collect($request->input()), $picture);

                $notification
                    = 'Permainan baru berhasil ditambahkan';
                return redirect()->route('back.game-index')
                    ->with('notification', $notification);
            } catch (ValidationException $e) {
                return redirect()->back()->withInput()
                    ->withErrors($e->errors());
            } catch (OptimisticLockException $e) {
                $notification
                    = 'Terjadi kesalahan, silahakan hubungi web administrator';
                return redirect()->route('back.game-index')
                    ->with('error_notification', $notification);
            }
        }
        return view('back.games.create');
    }

    /**
     * @param Request     $request
     * @param Games       $game
     * @param GameService $gameService
     *
     * @return Factory|RedirectResponse|View
     */
    public
    function edit(
        Request $request,
        Games $game,
        GameService $gameService
    ) {
        if ($request->method() === 'POST') {
            try {
                $this->validate($request, [
                    'title'       => 'required',
                    'history'     => 'required',
                    'philosophy'  => 'required',
                    'how-to-play' => 'required'
                ]);

                $picture = !is_null($request->file('image'));

                if ($picture) {
                    $picture = $gameService->upload($request, 'image');
                }
                $gameService->update($game, collect($request->input()),
                    $picture);

                $notification
                    = 'Permainan tradisional berhasil diperbaharui.';
                return redirect()->route('back.game-index')
                    ->with('notification', $notification);
            } catch (ValidationException $e) {
                return redirect()->back()->withInput()
                    ->withErrors($e->errors());
            } catch (Exception $e) {
                $notification
                    = 'Terjadi kesalahan, silahkan hubungi web administrator.';
                return redirect()->route('back.game-index')
                    ->with('notification', $notification);
            }
        }

        return view('back.games.edit', compact('game'));
    }

    /**
     * @param Games       $games
     * @param GameService $gameService
     *
     * @return RedirectResponse
     */
    public
    function delete(
        Games $games,
        GameService $gameService
    ) {
        try {
            $gameService->delete($games);

            $notification = 'Permainan berhasil dihapus.';

            return redirect()->route('back.game-index')
                ->with('notification', $notification);
        } catch (Exception $e) {
            $notification
                = 'Terjadi kesalahan, silahkan hubungi web administrator.';

            return redirect()->route('back.game-index')
                ->with('notification', $notification);
        }
    }

    /**
     * @param Games       $games
     * @param GameService $gameService
     *
     * @return Factory|View
     */
    public function view(
        Games $games,
        GameService $gameService
    ) {
        $game = $gameService->findGameById($games);

        return view('back.games.view', compact('game'));
    }
}
