<?php

namespace App\Http\Controllers\Front;

use App\Entities\Songs;
use App\Http\Controllers\Controller;
use App\Services\Domain\SongService;
use App\Services\Domain\ViewLogsService;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class SongController
 *
 * @package App\Http\Controllers\Front
 */
class SongController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('front.song.index');
    }

    /**
     * @param SongService $songService
     *
     * @return Factory|View
     */
    public function list(SongService $songService)
    {
        $songs = $songService->getAllSong();
        return view('front.song.list', compact('songs'));
    }

    /**
     * @param Songs           $songs
     *
     * @param ViewLogsService $viewLogsService
     *
     * @return Factory|View
     */
    public function view(
        Songs $songs,
        ViewLogsService $viewLogsService
    ) {
        $viewLogsService->create('songs', $songs->getId());
        return view('front.song.view', compact('songs'));
    }
}
