<?php

namespace App\Http\Controllers\Front;

use App\Entities\Stories;
use App\Http\Controllers\Controller;
use App\Services\Domain\StoryService;
use App\Services\Domain\ViewLogsService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class StoryController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('front.story.index');
    }

    /**
     * @param StoryService $storyService
     *
     * @return Factory|View
     */
    public function list(StoryService $storyService)
    {
        $stories = $storyService->getAllStory();
        return view('front.story.list', compact('stories'));
    }

    /**
     * @param Stories $story
     * @param Request $request
     * @param StoryService $storyService
     * @param ViewLogsService $viewLogsService
     * @return Factory|View
     */
    public function view(
        Stories $story,
        Request $request,
        StoryService $storyService,
        ViewLogsService $viewLogsService
    ) {
        $storyPage = $request->route('story_page');
        $viewLogsService->create('story', $story->getId());
        $story = $storyService->getPlotStory($story, $storyPage ?? 1);

        return view('front.story.view', compact('story'));
    }
}
