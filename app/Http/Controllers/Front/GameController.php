<?php

namespace App\Http\Controllers\Front;

use App\Entities\Games;
use App\Http\Controllers\Controller;
use App\Services\Domain\GameService;
use App\Services\Domain\ViewLogsService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class GameController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('front.game.index');
    }

    /**
     * @param GameService $gameService
     * @return Factory|View
     */
    public function list(GameService $gameService)
    {
        $games = $gameService->getAllGames();
        return view('front.game.list', compact('games'));
    }

    /**
     * @param Games $game
     * @param Request $request
     * @param GameService $gameService
     * @param ViewLogsService $viewLogsService
     * @return Factory|View
     */
    public function view(
        Games $game,
        Request $request,
        GameService $gameService,
        ViewLogsService $viewLogsService
    ) {
        $viewLogsService->create('games', $game->getId());

        return view('front.game.view', compact('game'));
    }
}
