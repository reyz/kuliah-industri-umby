<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Services\Domain\PageService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class PageController
 *
 * @package App\Http\Controllers\Front
 */
class PageController extends Controller
{
    /**
     * @param PageService $pageService
     *
     * @return Factory|View
     */
    public function index(PageService $pageService)
    {
        $pages = $pageService->getAllPage();
        return view('front.welcome', compact('pages'));
    }

    /**
     * @param Request     $request
     * @param PageService $pageService
     *
     * @return Factory|View
     */
    public function view(Request $request, PageService $pageService)
    {
        $slug = $request->route('halaman');
        $page = $pageService->getPageBySlug($slug);

        return view('front.pages.view', compact('page'));
    }
}
