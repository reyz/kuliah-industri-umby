<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SlugMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure                  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $storyTitle = $request->route('story_title');
        $storyTitle = strtolower(Str::slug($storyTitle));

        $request->route()->setParameter('story_title', $storyTitle);
        return $next($request);
    }
}
