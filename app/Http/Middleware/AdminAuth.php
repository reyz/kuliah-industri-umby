<?php

namespace App\Http\Middleware;

use App\Entities\UserInterface;
use App\Services\Application\UserService;
use Closure;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has('authenticated')) {
            if (session('authenticated')['type'] == 'administrator') {
                /** @var UserService $userService */
                $userService = app(UserService::class);
                $user = $userService->findUserById(session('authenticated')['id']);

                if ($user instanceof UserInterface) {
                    return $next($request);
                }
            }
        }

        return redirect()->route('back.login');
    }
}
