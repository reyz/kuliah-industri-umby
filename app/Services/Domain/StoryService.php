<?php

namespace App\Services\Domain;

use App\Entities\Stories;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\QueryBuilder;
use EntityManager;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use LaravelDoctrine\ORM\Pagination\PaginatesFromParams;

class StoryService
{
    use PaginatesFromParams;

    /**
     * Find Story by id
     *
     * @param $bankId
     *
     * @return object|Stories
     */
    public function findStoryById($bankId)
    {
        $bank = $this->getRepository()->find($bankId);

        return $bank;
    }

    /**
     * Instance repository
     *
     * @return ObjectRepository
     */
    public function getRepository()
    {
        return EntityManager::getRepository(Stories::class);
    }

    /**
     * @return array
     */
    public function getAllStory()
    {
        $stories = $this->getRepository()->findAll();

        return $stories;
    }

    public function getPlotByStory(Stories $stories)
    {
        $plots = app(PlotService::class);
        $result = $plots->getPlotAndAssetsByStory($stories);

        return $result;
    }

    /**
     * Paginate Story
     *
     * @param int $page
     *
     * @return LengthAwarePaginator
     */
    public function paginateStory($page): LengthAwarePaginator
    {
        $limit = 10;
        $query = $this->createQueryBuilder('k')
            ->getQuery();

        return $this->paginate($query, $limit, $page, false);
    }

    /**
     * @param      $alias
     * @param null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias, $indexBy = null)
    {
        return EntityManager::createQueryBuilder()
            ->select($alias)
            ->from(Stories::class, $alias, $indexBy);
    }

    /**
     * Create Story
     *
     * @param Collection $collection
     *
     */
    public function create(Collection $collection)
    {
        $story = new Stories();
        $story->setTitle($collection->get('title'));
        $story->setPublished($collection->get('published') === 'on' ? 1 : 0);

        EntityManager::persist($story);
        EntityManager::flush();
    }

    /**
     * Update Story
     *
     * @param Stories $story
     * @param Collection $collection
     *
     * @throws OptimisticLockException
     */
    public function update(
        Stories $story,
        Collection $collection
    )
    {
        $story->setTitle($collection->get('title'));
        $story->setPublished($collection->get('published') === 'on' ? 1 : 0);

        EntityManager::persist($story);
        EntityManager::flush();
    }

    /**
     * Delete Story
     *
     * @param Stories $story
     *
     * @throws OptimisticLockException
     */
    public function delete(Stories $story)
    {
        /** @var PlotService $plotService */
        $plotService = app(PlotService::class);
        $plotService->deleteByStory($story);

        EntityManager::remove($story);
        EntityManager::flush();
    }

    /**
     * @param Stories $story
     * @param int $page
     * @return object
     */
    public function getPlotStory(Stories $story, int $page)
    {
        /** @var PlotService $plotService */
        $plotService = app(PlotService::class);
        $result = $plotService->findPlotByStory($story->getId(), $page);

        return $result;
    }

    /**
     * @param string $title
     *
     * @return mixed
     * @throws NonUniqueResultException
     */
    private function getStoryIdFromTitle(string $title)
    {
        $title = str_replace('-', ' ', $title);
        $query = $this->createQueryBuilder('p')
            ->where('p.title = :title')
            ->setParameter('title', $title);

        $result = $query->getQuery()->getOneOrNullResult();

        return $result->getId();
    }
}
