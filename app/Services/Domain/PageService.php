<?php

namespace App\Services\Domain;

use App\Entities\Pages;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\QueryBuilder;
use EntityManager;
use Illuminate\Support\Collection;
use LaravelDoctrine\ORM\Pagination\PaginatesFromParams;
use Str;

/**
 * Class PageService
 *
 * @package App\Services\Domain
 */
class PageService
{
    use PaginatesFromParams;

    /**
     * @param      $alias
     * @param null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias, $indexBy = null)
    {
        return EntityManager::createQueryBuilder()
            ->select($alias)
            ->from(Pages::class, $alias, $indexBy);
    }

    /**
     * Find Page by id
     *
     * @param $id
     *
     * @return object|Pages
     */
    public function findPageById($id)
    {
        $bank = $this->getRepository()->find($id);

        return $bank;
    }

    /**
     * Instance repository
     *
     * @return EntityRepository
     */
    public function getRepository()
    {
        return EntityManager::getRepository(Pages::class);
    }

    /**
     * Paginate Page
     *
     * @return Pages[]
     */
    public function getAllPage()
    {
        $query = $this->getRepository()->findAll();

        return $query;
    }

    /**
     * Update Page
     *
     * @param Pages $page
     * @param Collection $collection
     *
     * @throws OptimisticLockException
     */
    public function update(Pages $page, Collection $collection)
    {
        $page->setTitle($collection->get('title'));
        $page->setContent($collection->get('description'));

        $this->save($page);
    }

    /**
     * @param Pages $pages
     *
     * @throws OptimisticLockException
     */
    private function save(Pages $pages)
    {
        EntityManager::persist($pages);
        EntityManager::flush();
    }

    /**
     * @param Collection $collection
     *
     * @throws OptimisticLockException
     */
    public function create(Collection $collection)
    {
        $page = new Pages();
        $page->setTitle($collection->get('title'));
        $page->setSlug(Str::slug($collection->get('title')));
        $page->setContent($collection->get('description'));
        $this->save($page);
    }

    /**
     * @param string $slug
     *
     * @return object|null
     */
    public function getPageBySlug(string $slug)
    {
        $page = $this->getRepository()->findOneBy(['slug' => $slug]);

        return $page;
    }
}
