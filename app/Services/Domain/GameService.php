<?php

namespace App\Services\Domain;

use App\Entities\Games;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\QueryBuilder;
use EntityManager;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use LaravelDoctrine\ORM\Pagination\PaginatesFromParams;

/**
 * Class GameService
 *
 * @package App\Services\Domain
 */
class GameService
{
    use PaginatesFromParams;

    /**
     * @param      $alias
     * @param null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias, $indexBy = null)
    {
        return EntityManager::createQueryBuilder()
            ->select($alias)
            ->from(Games::class, $alias, $indexBy);
    }

    /**
     * Paginate Game
     *
     * @return Games[]
     */
    public function getAllGames(): array
    {
        $query = $this->getRepository()->findAll();

        return $query;
    }

    /**
     * Instance repository
     *
     * @return ObjectRepository
     */
    public function getRepository()
    {
        return EntityManager::getRepository(Games::class);
    }

    /**
     * @param string $title
     *
     * @return object|null
     */
    public function getGameByTitle(string $title)
    {
        $title = str_replace('-', ' ', $title);
        $games = $this->getRepository()->findOneBy(['title' => $title]);

        return $games;
    }

    /**
     * @param Collection $collection
     *
     * @param string $picture
     * @throws OptimisticLockException
     */
    public function create(Collection $collection, string $picture): void
    {
        $game = new Games();
        $game->setTitle($collection->get('title'));
        $game->setHistory(htmlspecialchars($collection->get('history')));
        $game->setPhilosophy(htmlspecialchars($collection->get('philosophy')));
        $game->setHowToPlay(htmlspecialchars($collection->get('how-to-play')));
        $game->setPicture($picture);

        EntityManager::persist($game);
        EntityManager::flush();
    }

    /**
     * @param Games $games
     * @param Collection $collection
     *
     * @param $picture
     * @throws OptimisticLockException
     */
    public function update(Games $games, Collection $collection, $picture): void
    {
        $games->setTitle($collection->get('title'));
        $games->setHistory(htmlspecialchars($collection->get('history')));
        $games->setPhilosophy(htmlspecialchars($collection->get('philosophy')));
        $games->setHowToPlay(htmlspecialchars($collection->get('how-to-play')));

        if ($picture) {
            @unlink(public_path('/assets/uploads/' . $games->getPicture()));
            $games->setPicture($picture);
        }

        EntityManager::persist($games);
        EntityManager::flush();
    }

    /**
     * Find Page by id
     *
     * @param $id
     *
     * @return object|Games
     */
    public function findGameById($id)
    {
        $bank = $this->getRepository()->find($id);

        return $bank;
    }

    /**
     * @param Games $games
     *
     * @throws OptimisticLockException
     */
    public function delete(Games $games): void
    {
        @unlink(public_path('/assets/uploads/' . $games->getPicture()));
        EntityManager::remove($games);
        EntityManager::flush();
    }

    public function upload(Request $request, string $key)
    {
        $file = $request->file($key);

        $destinationPath = public_path('assets/uploads/songs/');
        $fileName = md5($file->getFilename()) . '.'
            . $file->getClientOriginalExtension();
        $file->move($destinationPath, $fileName);
        return $fileName;
    }
}
