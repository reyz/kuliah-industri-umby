<?php

namespace App\Services\Domain;

use App\Entities\Songs;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\QueryBuilder;
use EntityManager;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SongService
{
    /**
     * @param      $alias
     * @param null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias, $indexBy = null)
    {
        return EntityManager::createQueryBuilder()
            ->select($alias)
            ->from(Songs::class, $alias, $indexBy);
    }

    public function getAllSong()
    {
        $songs = $this->getRepository()->findAll();

        return $songs;
    }

    /**
     * Instance repository
     *
     * @return ObjectRepository
     */
    public function getRepository()
    {
        return EntityManager::getRepository(Songs::class);
    }

    /**
     * @param Collection $collection
     *
     * @param array $video
     *
     * @throws OptimisticLockException
     */
    public function create(Collection $collection, array $video)
    {
        $song = new Songs();
        $song->setTitle($collection->get('title'));
        $song->setDescription($collection->get('description'));
        $song->setVideo($video['filename']);
        $song->setType($video['type']);

        $this->save($song);
    }

    /**
     * @param Songs $song
     *
     * @throws OptimisticLockException
     */
    private function save(Songs $song)
    {
        EntityManager::persist($song);
        EntityManager::flush();
    }

    /**
     * @param Songs $songs
     *
     * @throws OptimisticLockException
     */
    public function delete(Songs $songs)
    {
        @unlink(public_path('assets/uploads/' . $songs->getVideo()));

        EntityManager::remove($songs);
        EntityManager::flush();
    }

    /**
     * Find Page by id
     *
     * @param $id
     *
     * @return object|Songs
     */
    public function findSongById($id)
    {
        $bank = $this->getRepository()->find($id);

        return $bank;
    }

    /**
     * @param Songs $songs
     * @param Collection $collection
     * @param array $video
     *
     * @throws OptimisticLockException
     */
    public function update(
        Songs $songs,
        Collection $collection,
        array $video
    )
    {
        $songs->setTitle($collection->get('title'));
        $songs->setDescription($collection->get('description'));

        if (!is_null($video)) {
            @unlink(public_path('/assets/uploads/' . $songs->getVideo()));
            $songs->setVideo($video['filename']);
            $songs->setType($video['type']);
        }

        $this->save($songs);
    }

    /**
     * @param Request $request
     * @param string $key
     *
     * @return array
     */
    public function upload(Request $request, string $key)
    {
        $file = $request->file($key);
        $destinationPath = public_path('assets/uploads/songs/');
        $fileName = md5($file->getFilename()) . '.'
            . $file->getClientOriginalExtension();

        $file->move($destinationPath, $fileName);
        return [
            'filename' => $fileName,
            'type' => $file->getClientMimeType()
        ];
    }
}
