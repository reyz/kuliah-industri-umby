<?php

namespace App\Services\Domain;

use App\Entities\Plots;
use App\Entities\Stories;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\QueryBuilder;
use EntityManager;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use LaravelDoctrine\ORM\Pagination\PaginatesFromParams;
use StdClass;

class PlotService
{
    use PaginatesFromParams;

    /**
     * Paginate Plot
     *
     * @param int $page
     *
     * @return LengthAwarePaginator
     */
    public function paginatePlot($page): LengthAwarePaginator
    {
        $limit = 10;
        $query = $this->createQueryBuilder('k')
            ->getQuery();

        return $this->paginate($query, $limit, $page, false);
    }

    /**
     * @param      $alias
     * @param null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias, $indexBy = null)
    {
        return EntityManager::createQueryBuilder()
            ->select($alias)
            ->from(Plots::class, $alias, $indexBy);
    }

    /**
     * @param Stories $stories
     * @param Collection $collection
     * @param            $picture
     *
     * @throws OptimisticLockException
     */
    public function create(
        Stories $stories,
        Collection $collection,
        $picture
    )
    {
        $lastPage = $this->getRepository()->findBy(['story' => $stories]);
        $lastPage = end($lastPage);

        $plot = new Plots();
        $plot->setPlotTitle($collection->get('plot_title'));
        $plot->setContent(htmlspecialchars($collection->get('content')));

        if (!$lastPage) {
            $plot->setPage(1);
        } else {
            $plot->setPage($lastPage->isPage() + 1);
        }
        $plot->setStory($stories);

        EntityManager::persist($plot);
        EntityManager::flush();

        if ($picture) {
            /** @var PlotAssetsService $assets */
            $assets = app(PlotAssetsService::class);
            $assets->create($plot, $picture);
        }

    }

    /**
     * Instance repository
     *
     */
    public function getRepository()
    {
        return EntityManager::getRepository(Plots::class);
    }

    /**
     * @param Plots $plot
     * @param Collection $collection
     * @param bool $picture
     *
     * @throws OptimisticLockException
     */
    public function update(
        Plots $plot,
        Collection $collection,
        $picture = false
    )
    {
        $plot->setPlotTitle($collection->get('plot_title'));
        $plot->setContent(htmlspecialchars($collection->get('content')));

        EntityManager::persist($plot);
        EntityManager::flush();

        if ($picture) {
            /** @var PlotAssetsService $assets */
            $assets = app(PlotAssetsService::class);
            $assets->update($plot, $picture);
        }
    }

    /**
     * Delete Plot
     *
     * @param Stories $stories
     * @param Plots $plot
     *
     * @throws OptimisticLockException
     */
    public function delete(Stories $stories, Plots $plot)
    {
        $this->updatePageOnDelete($stories, $plot);
        /** @var PlotAssetsService $assets */
        $assets = app(PlotAssetsService::class);
        $assets->deleteByPlot($plot);

        EntityManager::remove($plot);
        EntityManager::flush();
    }

    /**
     * @param Stories $stories
     * @param Plots $plots
     *
     * @throws OptimisticLockException
     */
    private function updatePageOnDelete(Stories $stories, Plots $plots)
    {
        $pagePlots = $this->createQueryBuilder('u')
            ->where('u.story = :story')
            ->andWhere('u.page > :page')
            ->setParameter('story', $stories->getId())
            ->setParameter('page', $plots->isPage())
            ->getQuery()
            ->getResult();

        foreach ($pagePlots as $plot) {
            /** @noinspection PhpUndefinedMethodInspection */
            $plot->setPage($plot->isPage() - 1);
            EntityManager::persist($plot);
        }
        EntityManager::flush();
    }

    /**
     * Upload Files
     *
     * @param Request $request
     * @param string $key
     *
     * @return string[]
     */
    public function upload(Request $request, string $key)
    {
        $file = $request->file($key);
        $destinationPath = public_path('assets/uploads/stories/');
        $mimeType = $file->getMimeType();
        $fileName = md5($file->getFilename()) . '.'
            . $file->getClientOriginalExtension();
        $file->move($destinationPath, $fileName);

        return [
            'fileName' => $fileName,
            'mimeType' => $mimeType
        ];
    }

    /**
     * @param Stories $stories
     * @param Plots $plots
     * @param int $newPage
     *
     * @throws OptimisticLockException
     */
    public function changePage(
        Stories $stories,
        Plots $plots,
        int $newPage
    )
    {
        $oldPage = $plots->isPage();
        $plots->setPage($newPage);

        EntityManager::persist($plots);
        EntityManager::flush();

        $this->changePageThatHaveNewPage($stories, $plots, $oldPage);

    }

    /**
     * @param Stories $stories
     * @param Plots $plots
     * @param int $oldPage
     *
     * @throws OptimisticLockException
     */
    private
    function changePageThatHaveNewPage(
        Stories $stories,
        Plots $plots,
        int $oldPage
    )
    {
        if ($oldPage < $plots->isPage()) {
            $plots = $this->createQueryBuilder('k')
                ->where('k.story = :story')
                ->andWhere('k.page <= :page')
                ->andWhere('k.id != :plot')
                ->setParameter('story', $stories)
                ->setParameter('page', $plots->isPage())
                ->setParameter('plot', $plots->getId())
                ->getQuery()
                ->getResult();
            foreach ($plots as $plot) {
                /** @noinspection PhpUndefinedMethodInspection */
                $plot->setPage($plot->isPage() - 1);
                EntityManager::persist($plot);
            }
        } else {
            $plots = $this->createQueryBuilder('k')
                ->where('k.story = :story')
                ->andWhere('k.page >= :page')
                ->andWhere('k.page < :oldPage')
                ->andWhere('k.id != :plot')
                ->setParameter('story', $stories)
                ->setParameter('page', $plots->isPage())
                ->setParameter('plot', $plots->getId())
                ->setParameter('oldPage', $oldPage)
                ->getQuery()
                ->getResult();

            foreach ($plots as $plot) {
                /** @noinspection PhpUndefinedMethodInspection */
                $plot->setPage($plot->isPage() + 1);
                EntityManager::persist($plot);
            }
        }
        EntityManager::flush();
    }

    /**
     * @param Plots $plots
     *
     * @return mixed
     */
    public function getPlotAndAsset(Plots $plots)
    {
        /** @var PlotAssetsService $plotAssets */
        $plotAssets = app(PlotAssetsService::class);
        $result = $plotAssets->findPlotAssetsByPlots($plots);

        return $result;
    }

    /**
     * @param Stories $stories
     *
     * @return array
     */
    public function getPlotAndAssetsByStory(Stories $stories)
    {
        /** @var Plots[] $plots */
        $plots = $this->getRepository()
            ->findBy(['story' => $stories], ['page' => 'asc']);

        $plotAssets = app(PlotAssetsService::class);

        $result = [];
        foreach ($plots as $plot) {
            $row = new StdClass;
            $row->plot = $plot;
            $row->assets = $plotAssets->getRepository()
                ->findOneBy(['plot' => $plot->getId()]);
            $result[] = $row;
        }

        return $result;
    }

    /**
     * @param Stories $story
     *
     * @throws OptimisticLockException
     */
    public
    function deleteByStory(
        Stories $story
    )
    {
        /** @var PlotAssetsService $plotAssetService */
        $plotAssetService = app(PlotAssetsService::class);
        $plots = $this->getRepository()->findBy(['story' => $story]);

        foreach ($plots as $plot) {
            $plotAssetService->deleteByPlot($plot);
            EntityManager::remove($plot);
        }

        EntityManager::flush();
    }

    public
    function findPlotByStory(
        int $id,
        int $page
    )
    {
        $countTotalPlot = $this->getRepository()->findBy([
            'story' => $id
        ]);

        $totalPage = count($countTotalPlot);
        $plot = $this->getRepository()->findOneBy([
            'story' => $id,
            'page' => $page
        ]);

        $assets = app(PlotAssetsService::class);
        $result = 0;
        if (!is_null($plot)) {
            $result = $assets->findPlotAssetsByPlots($plot);
        }

        return (object)[
            'result' => $result,
            'totalPage' => $totalPage
        ];
    }

    /**
     * Find Plot by id
     *
     * @param $bankId
     *
     * @return object|Plots
     */
    public
    function findPlotById(
        $bankId
    )
    {
        $bank = $this->getRepository()->find($bankId);

        return $bank;
    }
}
