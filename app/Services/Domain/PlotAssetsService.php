<?php

namespace App\Services\Domain;

use App\Entities\PlotAssets;
use App\Entities\Plots;
use Doctrine\ORM\QueryBuilder;
use EntityManager;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use LaravelDoctrine\ORM\Pagination\PaginatesFromParams;

class PlotAssetsService
{
    use PaginatesFromParams;

    /**
     * Paginate Plot
     *
     * @param int $page
     *
     * @return LengthAwarePaginator
     */
    public function paginatePlot($page): LengthAwarePaginator
    {
        $limit = 10;
        $query = $this->createQueryBuilder('k')
            ->getQuery();

        return $this->paginate($query, $limit, $page, false);
    }

    /**
     * @param      $alias
     * @param null $indexBy
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias, $indexBy = null)
    {
        return EntityManager::createQueryBuilder()
            ->select($alias)
            ->from(PlotAssets::class, $alias, $indexBy);
    }

    /**
     * Create Plot
     *
     * @param Plots $plots
     * @param bool $picture
     */
    public function create(Plots $plots, $picture = false)
    {
        $asset = new PlotAssets();
        $asset->setPlot($plots);
        $asset->setFilename($picture['fileName']);
        $asset->setType($picture['mimeType']);

        EntityManager::persist($asset);
        EntityManager::flush();
    }

    /**
     * Update Plot
     *
     * @param Plots $plots
     * @param bool $picture
     */
    public function update(
        Plots $plots,
        $picture = false
    )
    {
        $asset = $this->getRepository()->findOneBy(['plot' => $plots]);

        @unlink(public_path('/assets/uploads/' . $asset->getFilename()));

        $asset->setFilename($picture['fileName']);
        $asset->setType($picture['mimeType']);

        EntityManager::persist($asset);
        EntityManager::flush();
    }

    /**
     * Instance repository
     *
     */
    public function getRepository()
    {
        return EntityManager::getRepository(PlotAssets::class);
    }

    /**
     * Delete Plot
     *
     * @param PlotAssets $asset
     */
    public function delete(PlotAssets $asset)
    {
        EntityManager::remove($asset);
        EntityManager::flush();
    }

    /**
     * Upload Files
     *
     * @param Request $request
     * @param string $key
     *
     * @return string
     */
    public function upload(Request $request, string $key)
    {
        $file = $request->file($key);
        $destinationPath = public_path('assets/uploads/');
        $fileName = md5($file->getFilename()) . '.'
            . $file->getClientOriginalExtension();
        $file->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deleteByPlot(Plots $plot)
    {
        /** @var PlotAssets[] $assets */
        $assets = $this->getRepository()->findBy(['plot' => $plot]);

        foreach ($assets as $asset) {
            @unlink(public_path('/assets/uploads/'
                . $asset->getFilename()));
            EntityManager::remove($asset);
        }

        EntityManager::flush();
    }

    public function findPlotAssetsByPlots(Plots $plots)
    {
        $query = $this->getRepository()->findOneBy(['plot' => $plots]);

        return $query;
    }
}
