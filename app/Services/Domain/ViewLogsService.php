<?php

namespace App\Services\Domain;

use App\Entities\ViewLogs;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\QueryBuilder;
use EntityManager;

class ViewLogsService
{
    /**
     * Instance repository
     *
     * @return ObjectRepository
     */
    public function getRepository()
    {
        return EntityManager::getRepository(ViewLogs::class);
    }

    /**
     * @param $alias
     * @param null $indexBy
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias, $indexBy = null)
    {
        return EntityManager::createQueryBuilder()
            ->select($alias)
            ->from(ViewLogs::class, $alias, $indexBy);
    }

    /**
     * @param string $categoryText
     * @param int $contentId
     */
    public function create(string $categoryText, int $contentId)
    {
        $currentDate = date('Y-m-d H:i:s');
        $log = new ViewLogs();
        $log->setCategoryText($categoryText);
        $log->setContentId($contentId);
        $log->setCreatedAt($currentDate);

        EntityManager::persist($log);
        EntityManager::flush();
    }
}
