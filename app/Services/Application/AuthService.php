<?php

namespace App\Services\Application;

use App\Entities\Administrator;
use App\Entities\UserInterface;

class AuthService
{
    /**
     * Auth user
     *
     * @param $username
     * @param $password
     * @return bool
     */
    public function auth($username, $password)
    {
        /** @var UserService $userService */
        $userService = app(UserService::class);

        $user = $userService->findUserByUsername($username);

        if ($user instanceof UserInterface) {
            $validate = $userService->validatePassword($password, $user->getPassword());

            if ($validate) {
                \Log::info('[users] logged in');

                session()->put('authenticated', [
                    'id' => $user->getId(),
                    'type' => 'administrator'
                ]);

                return true;
            }

            \Log::info('[users] wrong password');

            return false;
        }

        \Log::info('[users] username not found');

        return false;
    }

    /**
     * Clear user session
     */
    public function logout()
    {
        session()->flush();
    }

    /**
     * Get logged user
     *
     * @return false|Administrator
     */
    public function getLoggedUser()
    {
        if (session()->has('authenticated')) {
            /** @var UserService $userService */
            $userService = app(UserService::class);
            $userId = session('authenticated')['id'] ?? false;

            if ($userId) {
                $user = $userService->findUserById($userId);

                if ($user instanceof UserInterface) {
                    return $user;
                }
            }
        }

        return false;
    }
}
