<?php

namespace App\Services\Application;

use App\Services\Domain\GameService;
use App\Services\Domain\SongService;
use App\Services\Domain\StoryService;
use DateTime;

class RankingService
{
    /**
     * @param bool $search
     * @param $startDate
     * @param $endDate
     * @param null $offset
     * @param null $limit
     * @return mixed
     */
    private function getByDate(
        $search,
        $startDate,
        $endDate
    ) {
        $data = [];
        $group = $this->getDataGroup($search, $startDate, $endDate);

        if (count($group)) {
            foreach ($group as $item) {
                $count = $this->countData($item->category_text, $item->content_id, $search, $startDate, $endDate);
                $data[] = [
                    'category' => $this->getCategoryTitle($item->category_text),
                    'content' => $this->getContent($item->category_text, $item->content_id)->getTitle(),
                    'count' => $count[0]->total
                ];
            }
        }

        usort($data, function($a, $b) {
                return $b['count'] <=> $a['count'];
            });
        $data = array_slice($data, 0, 10, true);

        return $data;
    }

    /**
     * @param $search
     * @param $startDate
     * @param $endDate
     * @return array
     */
    private function getDataGroup(
        $search,
        $startDate,
        $endDate
    )
    {
        if ($search == 'yes') {
            $from = new DateTime($startDate);
            $to = new DateTime($endDate);

            $query = \DB::select('select category_text, content_id from view_logs where date(created_at) between ? and ? GROUP BY category_text, content_id', [$from->format('Y-m-d'), $to->format('Y-m-d')]);
        } else {
            $query = \DB::select('select category_text, content_id from view_logs GROUP BY category_text, content_id');
        }

        return $query;
    }

    /**
     * @param $content
     * @param $id
     * @param $search
     * @param $startDate
     * @param $endDate
     * @return array
     */
    private function countData(
        $content,
        $id,
        $search,
        $startDate,
        $endDate
    )
    {
        if ($search == 'yes') {
            $from = new DateTime($startDate);
            $to = new DateTime($endDate);

            $query = \DB::select('select count(id) as total from view_logs where category_text = ? and content_id = ? and date(created_at) between ? and ?', [$content, $id, $from->format('Y-m-d'), $to->format('Y-m-d')]);
        } else {
            $query = \DB::select('select count(id) as total from view_logs where category_text = ? and content_id = ?', [$content, $id]);
        }

        return $query;
    }

    /**
     * @param $searchDate
     * @param null $startDate
     * @param null $endDate
     * @return array
     */
    public function getLogsData(
        $searchDate,
        $startDate = null,
        $endDate = null
    ) {
        $viewLogs = $this->getByDate($searchDate, $startDate, $endDate);

        $data = [];
        if (!empty($viewLogs)) {
            $no = 1;

            foreach ($viewLogs as $viewLog) {
                $nestedData['no'] = $no;
                $nestedData['category'] = $viewLog['category'];
                $nestedData['content'] = $viewLog['content'];
                $nestedData['count'] = $viewLog['count'];
                $data[] = $nestedData;

                $no++;
            }
        }

        return ['data' => $data, 'total' => count($data)];
    }

    /**
     * @param string $category
     * @param int $contentId
     * @return \Illuminate\Contracts\Foundation\Application|mixed|null
     */
    private function getCategoryTitle(string $category)
    {
        switch ($category) {
            case 'games':
                return 'Permainan Tradisional';
            case 'songs':
                return 'Lagu Dolanan';
            case 'story':
                return 'Dongeng';
        }
        return null;
    }

    /**
     * @param string $category
     * @param int $contentId
     * @return \Illuminate\Contracts\Foundation\Application|mixed|null
     */
    private function getContent(string $category, int $contentId)
    {
        switch ($category) {
            case 'games':
                $games = app(GameService::class);
                $games = $games->getRepository()
                    ->findOneBy(['id' => $contentId]);
                return $games;
            case 'songs':
                $songs = app(SongService::class);
                $songs = $songs->getRepository()
                    ->findOneBy(['id' => $contentId]);
                return $songs;
            case 'story':
                $story = app(StoryService::class);
                $story = $story->getRepository()
                    ->findOneBy(['id' => $contentId]);
                return $story;
        }
        return null;
    }
}
