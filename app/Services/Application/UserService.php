<?php

namespace App\Services\Application;

use App\Entities\Administrator;
use EntityManager;

class UserService
{
    /**
     * Instance repository admin
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepositoryAdmin()
    {
        return EntityManager::getRepository(Administrator::class);
    }

    /**
     * Find user by username
     *
     * @param $username
     * @return bool|Administrator
     */
    public function findUserByUsername($username)
    {
        $user = $this->getRepositoryAdmin()->findOneBy([
            'username' => $username
        ]);

        if ($user instanceof Administrator) {
            return $user;
        }

        return false;
    }

    /**
     * Check password
     *
     * @param $password
     * @param $hashedPassword
     * @return bool
     */
    public function validatePassword($password, $hashedPassword)
    {
        if (md5($password) == $hashedPassword) {
            return true;
        }

        return false;
    }

    /**
     * @param $userId
     * @return bool|Administrator
     */
    public function findUserById($userId)
    {
        $user = $this->getRepositoryAdmin()->find($userId);

        if ($user instanceof Administrator) {
            return $user;
        }

        return false;
    }
}
