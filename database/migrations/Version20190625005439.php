<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190625005439 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE plot_assets CHANGE type type VARCHAR(20) DEFAULT NULL');
        $this->addSql('ALTER TABLE plots CHANGE content content TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE games CHANGE history history TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE songs CHANGE description description VARCHAR(255) DEFAULT NULL, CHANGE type type VARCHAR(10) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE games CHANGE history history TEXT NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE plot_assets CHANGE type type VARCHAR(20) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE plots CHANGE content content TEXT NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE songs CHANGE description description VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE type type VARCHAR(10) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
