<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190514181802 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE plot_assets (id INT UNSIGNED AUTO_INCREMENT NOT NULL, plot_id INT UNSIGNED DEFAULT NULL, type VARCHAR(20) NOT NULL, filename VARCHAR(255) NOT NULL, INDEX plot_id_plots (plot_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE plots (id INT UNSIGNED AUTO_INCREMENT NOT NULL, story_id INT UNSIGNED DEFAULT NULL, plot_title VARCHAR(255) NOT NULL, content TEXT NOT NULL, page INT NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX story_id_stories (story_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE games (id INT UNSIGNED AUTO_INCREMENT NOT NULL, title VARCHAR(100) NOT NULL, history TEXT NOT NULL, philosophy TEXT NOT NULL, how_to_play TEXT NOT NULL, picture VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stories (id INT UNSIGNED AUTO_INCREMENT NOT NULL, title VARCHAR(100) NOT NULL, published TINYINT(1) NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE songs (id INT UNSIGNED AUTO_INCREMENT NOT NULL, title VARCHAR(50) NOT NULL, video VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, type VARCHAR(10) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE administrator (id INT UNSIGNED AUTO_INCREMENT NOT NULL, username VARCHAR(20) NOT NULL, password VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pages (id INT UNSIGNED AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, content TEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE plot_assets ADD CONSTRAINT FK_E830938D680D0B01 FOREIGN KEY (plot_id) REFERENCES plots (id)');
        $this->addSql('ALTER TABLE plots ADD CONSTRAINT FK_8FD44F00AA5D4036 FOREIGN KEY (story_id) REFERENCES stories (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE plot_assets DROP FOREIGN KEY FK_E830938D680D0B01');
        $this->addSql('ALTER TABLE plots DROP FOREIGN KEY FK_8FD44F00AA5D4036');
        $this->addSql('DROP TABLE plot_assets');
        $this->addSql('DROP TABLE plots');
        $this->addSql('DROP TABLE games');
        $this->addSql('DROP TABLE stories');
        $this->addSql('DROP TABLE songs');
        $this->addSql('DROP TABLE administrator');
        $this->addSql('DROP TABLE pages');
    }
}
