<?php

use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'title' => 'Tentang',
            'slug' => 'tentang',
            'content' => 'Media Pembelajaran sebagai alat yang menjadi perantara dalam menyampaikan pembelajaran pada anak. Prinsipnya, media yang akan digunakan tersebut dapt memberikan rangsangan semangat atau motivasi anak usia dini untuk dapat belajar dengan mudah dan menyenangkan sehingga mereka tidak merasa jenuh atau bosan dalam mengikuti proses pembelajaran. Program ini dibuat oleh Mahasiswa Universitas Mercu Buana Yogyakarta untuk menempuh mata kuliah "Kuliah Industri".'
        ]);

        DB::table('pages')->insert([
            'title' => 'Bantuan',
            'slug' => 'bantuan',
            'content' => 'Anda dapat memilih 3 kategori media: Lagu Daerah, Dongeng dan Permainan Tradisional.'
        ]);
    }
}
