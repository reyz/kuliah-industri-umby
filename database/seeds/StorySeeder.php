<?php

use Illuminate\Database\Seeder;

class StorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stories')->insert([
            [
                'id' => 1,
                'title' => 'Asal Mula Rumah Siput',
                'published' => true
            ],
            [
                'id' => 2,
                'title' => 'Ayam dan Musang',
                'published' => true
            ],
            [
                'id' => 3,
                'title' => 'Jarum Emas Elang',
                'published' => true
            ],
            [
                'id' => 4,
                'title' => 'Ayam Jantan dan Rubah',
                'published' => true
            ],
            [
                'id' => 5,
                'title' => 'Beruang dan Lebah',
                'published' => true
            ],
            [
                'id' => 6,
                'title' => 'Gagak yang Sombong',
                'published' => true
            ],
            [
                'id' => 7,
                'title' => 'Kambing Keras Kepala',
                'published' => true
            ],
            [
                'id' => 8,
                'title' => 'Katak dan Tikus',
                'published' => true
            ],
            [
                'id' => 9,
                'title' => 'Katak Kecil dan Ular Kecil',
                'published' => true
            ],
            [
                'id' => 10,
                'title' => 'Kelelawar Pengecut',
                'published' => true
            ]
        ]);

        // Story 1
        DB::table('plots')->insert([
            [
                'id' => 1,
                'story_id' => 1,
                'plot_title' => 'Halaman 1',
                'page' => '1'
            ],
            [
                'id' => 2,
                'story_id' => 1,
                'plot_title' => 'Halaman 2',
                'page' => '2'
            ],
            [
                'id' => 3,
                'story_id' => 1,
                'plot_title' => 'Halaman 3',
                'page' => '3'
            ],
            [
                'id' => 4,
                'story_id' => 1,
                'plot_title' => 'Halaman 4',
                'page' => '4'
            ],
            [
                'id' => 5,
                'story_id' => 1,
                'plot_title' => 'Halaman 5',
                'page' => '5'
            ],
            [
                'id' => 6,
                'story_id' => 1,
                'plot_title' => 'Halaman 6',
                'page' => '6'
            ],
            [
                'id' => 7,
                'story_id' => 1,
                'plot_title' => 'Halaman 7',
                'page' => '7'
            ],
            [
                'id' => 8,
                'story_id' => 1,
                'plot_title' => 'Halaman 8',
                'page' => '8'
            ],
            [
                'id' => 9,
                'story_id' => 1,
                'plot_title' => 'Halaman 9',
                'page' => '9'
            ],
            [
                'id' => 10,
                'story_id' => 1,
                'plot_title' => 'Halaman 10',
                'page' => '10'
            ],
            [
                'id' => 11,
                'story_id' => 1,
                'plot_title' => 'Halaman 11',
                'page' => '11'
            ],
            [
                'id' => 12,
                'story_id' => 1,
                'plot_title' => 'Halaman 12',
                'page' => '12'
            ],
        ]);

        DB::table('plot_assets')->insert([
            [
                'plot_id' => 1,
                'filename' => 'asal_mula_rumah_siput_1.png'
            ],
            [
                'plot_id' => 2,
                'filename' => 'asal_mula_rumah_siput_2.png'
            ],
            [
                'plot_id' => 3,
                'filename' => 'asal_mula_rumah_siput_3.png'
            ],
            [
                'plot_id' => 4,
                'filename' => 'asal_mula_rumah_siput_4.png'
            ],
            [
                'plot_id' => 5,
                'filename' => 'asal_mula_rumah_siput_5.png'
            ],
            [
                'plot_id' => 6,
                'filename' => 'asal_mula_rumah_siput_6.png'
            ],
            [
                'plot_id' => 7,
                'filename' => 'asal_mula_rumah_siput_7.png'
            ],
            [
                'plot_id' => 8,
                'filename' => 'asal_mula_rumah_siput_8.png'
            ],
            [
                'plot_id' => 9,
                'filename' => 'asal_mula_rumah_siput_9.png'
            ],
            [
                'plot_id' => 10,
                'filename' => 'asal_mula_rumah_siput_10.png'
            ],
            [
                'plot_id' => 11,
                'filename' => 'asal_mula_rumah_siput_11.png'
            ],
            [
                'plot_id' => 12,
                'filename' => 'asal_mula_rumah_siput_12.png'
            ]
        ]);

        // Story 2
        DB::table('plots')->insert([
            [
                'id' => 13,
                'story_id' => 2,
                'plot_title' => 'Halaman 1',
                'page' => '1'
            ],
            [
                'id' => 14,
                'story_id' => 2,
                'plot_title' => 'Halaman 2',
                'page' => '2'
            ],
            [
                'id' => 15,
                'story_id' => 2,
                'plot_title' => 'Halaman 3',
                'page' => '3'
            ],
            [
                'id' => 16,
                'story_id' => 2,
                'plot_title' => 'Halaman 4',
                'page' => '4'
            ],
            [
                'id' => 17,
                'story_id' => 2,
                'plot_title' => 'Halaman 5',
                'page' => '5'
            ],
            [
                'id' => 18,
                'story_id' => 2,
                'plot_title' => 'Halaman 6',
                'page' => '6'
            ],
            [
                'id' => 19,
                'story_id' => 2,
                'plot_title' => 'Halaman 7',
                'page' => '7'
            ],
            [
                'id' => 20,
                'story_id' => 2,
                'plot_title' => 'Halaman 8',
                'page' => '8'
            ],
            [
                'id' => 21,
                'story_id' => 2,
                'plot_title' => 'Halaman 9',
                'page' => '9'
            ],
            [
                'id' => 22,
                'story_id' => 2,
                'plot_title' => 'Halaman 10',
                'page' => '10'
            ],
            [
                'id' => 23,
                'story_id' => 2,
                'plot_title' => 'Halaman 11',
                'page' => '11'
            ],
            [
                'id' => 24,
                'story_id' => 2,
                'plot_title' => 'Halaman 12',
                'page' => '12'
            ],
        ]);

        DB::table('plot_assets')->insert([
            [
                'plot_id' => 13,
                'filename' => 'ayam_dan_musang_1.png'
            ],
            [
                'plot_id' => 14,
                'filename' => 'ayam_dan_musang_2.png'
            ],
            [
                'plot_id' => 15,
                'filename' => 'ayam_dan_musang_3.png'
            ],
            [
                'plot_id' => 16,
                'filename' => 'ayam_dan_musang_4.png'
            ],
            [
                'plot_id' => 17,
                'filename' => 'ayam_dan_musang_5.png'
            ],
            [
                'plot_id' => 18,
                'filename' => 'ayam_dan_musang_6.png'
            ],
            [
                'plot_id' => 19,
                'filename' => 'ayam_dan_musang_7.png'
            ],
            [
                'plot_id' => 20,
                'filename' => 'ayam_dan_musang_8.png'
            ],
            [
                'plot_id' => 21,
                'filename' => 'ayam_dan_musang_9.png'
            ],
            [
                'plot_id' => 22,
                'filename' => 'ayam_dan_musang_10.png'
            ],
            [
                'plot_id' => 23,
                'filename' => 'ayam_dan_musang_11.png'
            ],
            [
                'plot_id' => 24,
                'filename' => 'ayam_dan_musang_12.png'
            ]
        ]);

        // Story 3
        DB::table('plots')->insert([
            [
                'id' => 25,
                'story_id' => 3,
                'plot_title' => 'Halaman 1',
                'page' => '1'
            ],
            [
                'id' => 26,
                'story_id' => 3,
                'plot_title' => 'Halaman 2',
                'page' => '2'
            ],
            [
                'id' => 27,
                'story_id' => 3,
                'plot_title' => 'Halaman 3',
                'page' => '3'
            ],
            [
                'id' => 28,
                'story_id' => 3,
                'plot_title' => 'Halaman 4',
                'page' => '4'
            ],
            [
                'id' => 29,
                'story_id' => 3,
                'plot_title' => 'Halaman 5',
                'page' => '5'
            ],
            [
                'id' => 30,
                'story_id' => 3,
                'plot_title' => 'Halaman 6',
                'page' => '6'
            ],
            [
                'id' => 31,
                'story_id' => 3,
                'plot_title' => 'Halaman 7',
                'page' => '7'
            ],
            [
                'id' => 32,
                'story_id' => 3,
                'plot_title' => 'Halaman 8',
                'page' => '8'
            ],
            [
                'id' => 33,
                'story_id' => 3,
                'plot_title' => 'Halaman 9',
                'page' => '9'
            ],
            [
                'id' => 34,
                'story_id' => 3,
                'plot_title' => 'Halaman 10',
                'page' => '10'
            ],
            [
                'id' => 35,
                'story_id' => 3,
                'plot_title' => 'Halaman 11',
                'page' => '11'
            ],
            [
                'id' => 36,
                'story_id' => 3,
                'plot_title' => 'Halaman 12',
                'page' => '12'
            ],
            [
                'id' => 37,
                'story_id' => 3,
                'plot_title' => 'Halaman 13',
                'page' => '13'
            ],
            [
                'id' => 38,
                'story_id' => 3,
                'plot_title' => 'Halaman 14',
                'page' => '14'
            ],
            [
                'id' => 39,
                'story_id' => 3,
                'plot_title' => 'Halaman 15',
                'page' => '15'
            ],
            [
                'id' => 40,
                'story_id' => 3,
                'plot_title' => 'Halaman 16',
                'page' => '16'
            ],
            [
                'id' => 41,
                'story_id' => 3,
                'plot_title' => 'Halaman 17',
                'page' => '17'
            ],
            [
                'id' => 42,
                'story_id' => 3,
                'plot_title' => 'Halaman 18',
                'page' => '18'
            ],
            [
                'id' => 43,
                'story_id' => 3,
                'plot_title' => 'Halaman 19',
                'page' => '19'
            ],
            [
                'id' => 44,
                'story_id' => 3,
                'plot_title' => 'Halaman 20',
                'page' => '20'
            ],
            [
                'id' => 45,
                'story_id' => 3,
                'plot_title' => 'Halaman 21',
                'page' => '21'
            ],
            [
                'id' => 46,
                'story_id' => 3,
                'plot_title' => 'Halaman 22',
                'page' => '22'
            ],
            [
                'id' => 47,
                'story_id' => 3,
                'plot_title' => 'Halaman 23',
                'page' => '23'
            ],
            [
                'id' => 48,
                'story_id' => 3,
                'plot_title' => 'Halaman 24',
                'page' => '24'
            ],
            [
                'id' => 49,
                'story_id' => 3,
                'plot_title' => 'Halaman 25',
                'page' => '25'
            ],
            [
                'id' => 50,
                'story_id' => 3,
                'plot_title' => 'Halaman 26',
                'page' => '26'
            ],
            [
                'id' => 51,
                'story_id' => 3,
                'plot_title' => 'Halaman 27',
                'page' => '27'
            ],
            [
                'id' => 52,
                'story_id' => 3,
                'plot_title' => 'Halaman 28',
                'page' => '28'
            ],
            [
                'id' => 53,
                'story_id' => 3,
                'plot_title' => 'Halaman 29',
                'page' => '29'
            ],
            [
                'id' => 54,
                'story_id' => 3,
                'plot_title' => 'Halaman 30',
                'page' => '30'
            ],
            [
                'id' => 55,
                'story_id' => 3,
                'plot_title' => 'Halaman 31',
                'page' => '31'
            ],
            [
                'id' => 56,
                'story_id' => 3,
                'plot_title' => 'Halaman 32',
                'page' => '32'
            ],
            [
                'id' => 57,
                'story_id' => 3,
                'plot_title' => 'Halaman 33',
                'page' => '33'
            ],
            [
                'id' => 58,
                'story_id' => 3,
                'plot_title' => 'Halaman 34',
                'page' => '34'
            ],
            [
                'id' => 59,
                'story_id' => 3,
                'plot_title' => 'Halaman 35',
                'page' => '35'
            ],
            [
                'id' => 60,
                'story_id' => 3,
                'plot_title' => 'Halaman 36',
                'page' => '36'
            ]
        ]);

        DB::table('plot_assets')->insert([
            [
                'plot_id' => 25,
                'filename' => 'ayam_jago_dan_jarum_emasburung_elang__1.png'
            ],
            [
                'plot_id' => 26,
                'filename' => 'ayam_jago_dan_jarum_emasburung_elang__2.png'
            ],
            [
                'plot_id' => 27,
                'filename' => 'ayam_jago_dan_jarum_emasburung_elang_3.png'
            ],
            [
                'plot_id' => 28,
                'filename' => 'ayam_jago_dan_jarum_emasburung_elang_4.png'
            ],
            [
                'plot_id' => 29,
                'filename' => 'ayam_jago_dan_jarum_emasburung_elang_5.png'
            ],
            [
                'plot_id' => 30,
                'filename' => 'ayam_jago_dan_jarum_emasburung_elang_6.png'
            ],
            [
                'plot_id' => 31,
                'filename' => 'ayam_jago_dan_jarum_emasburung_elang_7.png'
            ],
            [
                'plot_id' => 32,
                'filename' => 'ayam_jago_dan_jarum_emasburung_elang_8.png'
            ],
            [
                'plot_id' => 33,
                'filename' => 'ayam_jago_dan_jarum_emasburung_elang_9.png'
            ],
            [
                'plot_id' => 34,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_10.png'
            ],
            [
                'plot_id' => 35,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_11.png'
            ],
            [
                'plot_id' => 36,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_12.png'
            ],
            [
                'plot_id' => 37,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_13.png'
            ],
            [
                'plot_id' => 38,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_14.png'
            ],
            [
                'plot_id' => 39,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_15.png'
            ],
            [
                'plot_id' => 40,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_16.png'
            ],
            [
                'plot_id' => 41,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_17.png'
            ],
            [
                'plot_id' => 42,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_18.png'
            ],
            [
                'plot_id' => 43,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_19.png'
            ],
            [
                'plot_id' => 44,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_20.png'
            ],
            [
                'plot_id' => 45,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_21.png'
            ],
            [
                'plot_id' => 46,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_22.png'
            ],
            [
                'plot_id' => 47,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_23.png'
            ],
            [
                'plot_id' => 48,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_24.png'
            ],
            [
                'plot_id' => 49,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_25.png'
            ],
            [
                'plot_id' => 50,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_26.png'
            ],
            [
                'plot_id' => 51,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_27.png'
            ],
            [
                'plot_id' => 52,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_28.png'
            ],
            [
                'plot_id' => 53,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_29.png'
            ],
            [
                'plot_id' => 54,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_30.png'
            ],
            [
                'plot_id' => 55,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_31.png'
            ],
            [
                'plot_id' => 56,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_32.png'
            ],
            [
                'plot_id' => 57,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_33.png'
            ],
            [
                'plot_id' => 58,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_34.png'
            ],
            [
                'plot_id' => 59,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_35.png'
            ],
            [
                'plot_id' => 60,
                'filename' => 'ayam_jago_dan_jarum_emas_burung_elang_36.png'
            ]
        ]);

        // Story 4
        DB::table('plots')->insert([
            [
                'id' => 61,
                'story_id' => 4,
                'plot_title' => 'Halaman 1',
                'page' => '1'
            ],
            [
                'id' => 62,
                'story_id' => 4,
                'plot_title' => 'Halaman 2',
                'page' => '2'
            ],
            [
                'id' => 63,
                'story_id' => 4,
                'plot_title' => 'Halaman 3',
                'page' => '3'
            ],
            [
                'id' => 64,
                'story_id' => 4,
                'plot_title' => 'Halaman 4',
                'page' => '4'
            ],
            [
                'id' => 65,
                'story_id' => 4,
                'plot_title' => 'Halaman 5',
                'page' => '5'
            ],
            [
                'id' => 66,
                'story_id' => 4,
                'plot_title' => 'Halaman 6',
                'page' => '6'
            ],
            [
                'id' => 67,
                'story_id' => 4,
                'plot_title' => 'Halaman 7',
                'page' => '7'
            ],
            [
                'id' => 68,
                'story_id' => 4,
                'plot_title' => 'Halaman 8',
                'page' => '8'
            ],
            [
                'id' => 69,
                'story_id' => 4,
                'plot_title' => 'Halaman 9',
                'page' => '9'
            ],
            [
                'id' => 70,
                'story_id' => 4,
                'plot_title' => 'Halaman 10',
                'page' => '10'
            ],
            [
                'id' => 71,
                'story_id' => 4,
                'plot_title' => 'Halaman 11',
                'page' => '11'
            ],
        ]);

        DB::table('plot_assets')->insert([
            [
                'plot_id' => 61,
                'filename' => 'ayam_jantan_yang_cerdik_dan_rubah_yang_licik_1.png'
            ],
            [
                'plot_id' => 62,
                'filename' => 'ayam_jantan_yang_cerdik_dan_rubah_yang_licik_2.png'
            ],
            [
                'plot_id' => 63,
                'filename' => 'ayam_jantan_yang_cerdik_dan_rubah_yang_licik_3.png'
            ],
            [
                'plot_id' => 64,
                'filename' => 'ayam_jantan_yang_cerdik_dan_rubah_yang_licik_4.png'
            ],
            [
                'plot_id' => 65,
                'filename' => 'ayam_jantan_yang_cerdik_dan_rubah_yang_licik_5.png'
            ],
            [
                'plot_id' => 66,
                'filename' => 'ayam_jantan_yang_cerdik_dan_rubah_yang_licik_6.png'
            ],
            [
                'plot_id' => 67,
                'filename' => 'ayam_jantan_yang_cerdik_dan_rubah_yang_licik_7.png'
            ],
            [
                'plot_id' => 68,
                'filename' => 'ayam_jantan_yang_cerdik_dan_rubah_yang_licik_8.png'
            ],
            [
                'plot_id' => 69,
                'filename' => 'ayam_jantan_yang_cerdik_dan_rubah_yang_licik_9.png'
            ],
            [
                'plot_id' => 70,
                'filename' => 'ayam_jantan_yang_cerdik_dan_rubah_yang_licik_10.png'
            ],
            [
                'plot_id' => 71,
                'filename' => 'ayam_jantan_yang_cerdik_dan_rubah_yang_licik_11.png'
            ],
        ]);

        // Story 5
        DB::table('plots')->insert([
            [
                'id' => 72,
                'story_id' => 5,
                'plot_title' => 'Halaman 1',
                'page' => '1'
            ],
            [
                'id' => 73,
                'story_id' => 5,
                'plot_title' => 'Halaman 2',
                'page' => '2'
            ],
            [
                'id' => 74,
                'story_id' => 5,
                'plot_title' => 'Halaman 3',
                'page' => '3'
            ],
            [
                'id' => 75,
                'story_id' => 5,
                'plot_title' => 'Halaman 4',
                'page' => '4'
            ],
            [
                'id' => 76,
                'story_id' => 5,
                'plot_title' => 'Halaman 5',
                'page' => '5'
            ],
            [
                'id' => 77,
                'story_id' => 5,
                'plot_title' => 'Halaman 6',
                'page' => '6'
            ],
            [
                'id' => 78,
                'story_id' => 5,
                'plot_title' => 'Halaman 7',
                'page' => '7'
            ],
            [
                'id' => 79,
                'story_id' => 5,
                'plot_title' => 'Halaman 8',
                'page' => '8'
            ],
            [
                'id' => 80,
                'story_id' => 5,
                'plot_title' => 'Halaman 9',
                'page' => '9'
            ],
            [
                'id' => 81,
                'story_id' => 5,
                'plot_title' => 'Halaman 10',
                'page' => '10'
            ],
            [
                'id' => 82,
                'story_id' => 5,
                'plot_title' => 'Halaman 11',
                'page' => '11'
            ],
            [
                'id' => 83,
                'story_id' => 5,
                'plot_title' => 'Halaman 12',
                'page' => '12'
            ],
        ]);

        DB::table('plot_assets')->insert([
            [
                'plot_id' => 72,
                'filename' => 'beruang_dan_lebah_1.png'
            ],
            [
                'plot_id' => 73,
                'filename' => 'beruang_dan_lebah_2.png'
            ],
            [
                'plot_id' => 74,
                'filename' => 'beruang_dan_lebah_3.png'
            ],
            [
                'plot_id' => 75,
                'filename' => 'beruang_dan_lebah_4.png'
            ],
            [
                'plot_id' => 76,
                'filename' => 'beruang_dan_lebah_5.png'
            ],
            [
                'plot_id' => 77,
                'filename' => 'beruang_dan_lebah_6.png'
            ],
            [
                'plot_id' => 78,
                'filename' => 'beruang_dan_lebah_7.png'
            ],
            [
                'plot_id' => 79,
                'filename' => 'beruang_dan_lebah_8.png'
            ],
            [
                'plot_id' => 80,
                'filename' => 'beruang_dan_lebah_9.png'
            ],
            [
                'plot_id' => 81,
                'filename' => 'beruang_dan_lebah_10.png'
            ],
            [
                'plot_id' => 82,
                'filename' => 'beruang_dan_lebah_11.png'
            ],
            [
                'plot_id' => 83,
                'filename' => 'beruang_dan_lebah_12.png'
            ]
        ]);

        // Story 6
        DB::table('plots')->insert([
            [
                'id' => 84,
                'story_id' => 6,
                'plot_title' => 'Halaman 1',
                'page' => '1'
            ],
            [
                'id' => 85,
                'story_id' => 6,
                'plot_title' => 'Halaman 2',
                'page' => '2'
            ],
            [
                'id' => 86,
                'story_id' => 6,
                'plot_title' => 'Halaman 3',
                'page' => '3'
            ],
            [
                'id' => 87,
                'story_id' => 6,
                'plot_title' => 'Halaman 4',
                'page' => '4'
            ],
            [
                'id' => 88,
                'story_id' => 6,
                'plot_title' => 'Halaman 5',
                'page' => '5'
            ],
            [
                'id' => 89,
                'story_id' => 6,
                'plot_title' => 'Halaman 6',
                'page' => '6'
            ],
            [
                'id' => 90,
                'story_id' => 6,
                'plot_title' => 'Halaman 7',
                'page' => '7'
            ],
            [
                'id' => 91,
                'story_id' => 6,
                'plot_title' => 'Halaman 8',
                'page' => '8'
            ],
            [
                'id' => 92,
                'story_id' => 6,
                'plot_title' => 'Halaman 9',
                'page' => '9'
            ]
        ]);

        DB::table('plot_assets')->insert([
            [
                'plot_id' => 84,
                'filename' => 'gagak_yang_sombong_1.png'
            ],
            [
                'plot_id' => 85,
                'filename' => 'gagak_yang_sombong_2.png'
            ],
            [
                'plot_id' => 86,
                'filename' => 'gagak_yang_sombong_3.png'
            ],
            [
                'plot_id' => 87,
                'filename' => 'gagak_yang_sombong_4.png'
            ],
            [
                'plot_id' => 88,
                'filename' => 'gagak_yang_sombong_5.png'
            ],
            [
                'plot_id' => 89,
                'filename' => 'gagak_yang_sombong_6.png'
            ],
            [
                'plot_id' => 90,
                'filename' => 'gagak_yang_sombong_7.png'
            ],
            [
                'plot_id' => 91,
                'filename' => 'gagak_yang_sombong_8.png'
            ],
            [
                'plot_id' => 92,
                'filename' => 'gagak_yang_sombong_9.png'
            ]
        ]);

        // Story 7
        DB::table('plots')->insert([
            [
                'id' => 93,
                'story_id' => 7,
                'plot_title' => 'Halaman 1',
                'page' => '1'
            ],
            [
                'id' => 94,
                'story_id' => 7,
                'plot_title' => 'Halaman 2',
                'page' => '2'
            ],
            [
                'id' => 95,
                'story_id' => 7,
                'plot_title' => 'Halaman 3',
                'page' => '3'
            ],
            [
                'id' => 96,
                'story_id' => 7,
                'plot_title' => 'Halaman 4',
                'page' => '4'
            ],
            [
                'id' => 97,
                'story_id' => 7,
                'plot_title' => 'Halaman 5',
                'page' => '5'
            ],
            [
                'id' => 98,
                'story_id' => 7,
                'plot_title' => 'Halaman 6',
                'page' => '6'
            ],
            [
                'id' => 99,
                'story_id' => 7,
                'plot_title' => 'Halaman 7',
                'page' => '7'
            ],
            [
                'id' => 100,
                'story_id' => 7,
                'plot_title' => 'Halaman 8',
                'page' => '8'
            ],
            [
                'id' => 101,
                'story_id' => 7,
                'plot_title' => 'Halaman 9',
                'page' => '9'
            ]
        ]);

        DB::table('plot_assets')->insert([
            [
                'plot_id' => 93,
                'filename' => 'kambing_yang_keras_kepala_1.png'
            ],
            [
                'plot_id' => 94,
                'filename' => 'kambing_yang_keras_kepala_2.png'
            ],
            [
                'plot_id' => 95,
                'filename' => 'kambing_yang_keras_kepala_3.png'
            ],
            [
                'plot_id' => 96,
                'filename' => 'kambing_yang_keras_kepala_4.png'
            ],
            [
                'plot_id' => 97,
                'filename' => 'kambing_yang_keras_kepala_5.png'
            ],
            [
                'plot_id' => 98,
                'filename' => 'kambing_yang_keras_kepala_6.png'
            ],
            [
                'plot_id' => 99,
                'filename' => 'kambing_yang_keras_kepala_7.png'
            ],
            [
                'plot_id' => 100,
                'filename' => 'kambing_yang_keras_kepala_8.png'
            ],
            [
                'plot_id' => 101,
                'filename' => 'kambing_yang_keras_kepala_9.png'
            ]
        ]);

        // Story 8
        DB::table('plots')->insert([
            [
                'id' => 102,
                'story_id' => 8,
                'plot_title' => 'Halaman 1',
                'page' => '1'
            ],
            [
                'id' => 103,
                'story_id' => 8,
                'plot_title' => 'Halaman 2',
                'page' => '2'
            ],
            [
                'id' => 104,
                'story_id' => 8,
                'plot_title' => 'Halaman 3',
                'page' => '3'
            ],
            [
                'id' => 105,
                'story_id' => 8,
                'plot_title' => 'Halaman 4',
                'page' => '4'
            ],
            [
                'id' => 106,
                'story_id' => 8,
                'plot_title' => 'Halaman 5',
                'page' => '5'
            ],
            [
                'id' => 107,
                'story_id' => 8,
                'plot_title' => 'Halaman 6',
                'page' => '6'
            ],
            [
                'id' => 108,
                'story_id' => 8,
                'plot_title' => 'Halaman 7',
                'page' => '7'
            ]
        ]);

        DB::table('plot_assets')->insert([
            [
                'plot_id' => 102,
                'filename' => 'katak_dan_tikus_1.png'
            ],
            [
                'plot_id' => 103,
                'filename' => 'katak_dan_tikus_2.png'
            ],
            [
                'plot_id' => 104,
                'filename' => 'katak_dan_tikus_3.png'
            ],
            [
                'plot_id' => 105,
                'filename' => 'katak_dan_tikus_4.png'
            ],
            [
                'plot_id' => 106,
                'filename' => 'katak_dan_tikus_5.png'
            ],
            [
                'plot_id' => 107,
                'filename' => 'katak_dan_tikus_6.png'
            ],
            [
                'plot_id' => 108,
                'filename' => 'katak_dan_tikus_7.png'
            ]
        ]);

        // Story 9
        DB::table('plots')->insert([
            [
                'id' => 109,
                'story_id' => 9,
                'plot_title' => 'Halaman 1',
                'page' => '1'
            ],
            [
                'id' => 110,
                'story_id' => 9,
                'plot_title' => 'Halaman 2',
                'page' => '2'
            ],
            [
                'id' => 111,
                'story_id' => 9,
                'plot_title' => 'Halaman 3',
                'page' => '3'
            ],
            [
                'id' => 112,
                'story_id' => 9,
                'plot_title' => 'Halaman 4',
                'page' => '4'
            ],
            [
                'id' => 113,
                'story_id' => 9,
                'plot_title' => 'Halaman 5',
                'page' => '5'
            ],
            [
                'id' => 114,
                'story_id' => 9,
                'plot_title' => 'Halaman 6',
                'page' => '6'
            ],
            [
                'id' => 115,
                'story_id' => 9,
                'plot_title' => 'Halaman 7',
                'page' => '7'
            ],
            [
                'id' => 116,
                'story_id' => 9,
                'plot_title' => 'Halaman 8',
                'page' => '8'
            ],
            [
                'id' => 117,
                'story_id' => 9,
                'plot_title' => 'Halaman 9',
                'page' => '9'
            ],
            [
                'id' => 118,
                'story_id' => 9,
                'plot_title' => 'Halaman 10',
                'page' => '10'
            ],
            [
                'id' => 119,
                'story_id' => 9,
                'plot_title' => 'Halaman 11',
                'page' => '11'
            ],
            [
                'id' => 120,
                'story_id' => 9,
                'plot_title' => 'Halaman 12',
                'page' => '12'
            ],
            [
                'id' => 121,
                'story_id' => 9,
                'plot_title' => 'Halaman 13',
                'page' => '13'
            ],
            [
                'id' => 122,
                'story_id' => 9,
                'plot_title' => 'Halaman 14',
                'page' => '14'
            ],
            [
                'id' => 123,
                'story_id' => 9,
                'plot_title' => 'Halaman 15',
                'page' => '15'
            ],
            [
                'id' => 124,
                'story_id' => 9,
                'plot_title' => 'Halaman 16',
                'page' => '16'
            ]
        ]);

        DB::table('plot_assets')->insert([
            [
                'plot_id' => 109,
                'filename' => 'katak_kecil_dan_ular_kecil_1.png'
            ],
            [
                'plot_id' => 110,
                'filename' => 'katak_kecil_dan_ular_kecil_2.png'
            ],
            [
                'plot_id' => 111,
                'filename' => 'katak_kecil_dan_ular_kecil_3.png'
            ],
            [
                'plot_id' => 112,
                'filename' => 'katak_kecil_dan_ular_kecil_4.png'
            ],
            [
                'plot_id' => 113,
                'filename' => 'katak_kecil_dan_ular_kecil_5.png'
            ],
            [
                'plot_id' => 114,
                'filename' => 'katak_kecil_dan_ular_kecil_6.png'
            ],
            [
                'plot_id' => 115,
                'filename' => 'katak_kecil_dan_ular_kecil_7.png'
            ],
            [
                'plot_id' => 116,
                'filename' => 'katak_kecil_dan_ular_kecil_8.png'
            ],
            [
                'plot_id' => 117,
                'filename' => 'katak_kecil_dan_ular_kecil_9.png'
            ],
            [
                'plot_id' => 118,
                'filename' => 'katak_kecil_dan_ular_kecil_10.png'
            ],
            [
                'plot_id' => 119,
                'filename' => 'katak_kecil_dan_ular_kecil_11.png'
            ],
            [
                'plot_id' => 120,
                'filename' => 'katak_kecil_dan_ular_kecil_12.png'
            ],
            [
                'plot_id' => 121,
                'filename' => 'katak_kecil_dan_ular_kecil_13.png'
            ],
            [
                'plot_id' => 122,
                'filename' => 'katak_kecil_dan_ular_kecil_14.png'
            ],
            [
                'plot_id' => 123,
                'filename' => 'katak_kecil_dan_ular_kecil_15.png'
            ],
            [
                'plot_id' => 124,
                'filename' => 'katak_kecil_dan_ular_kecil_16.png'
            ]
        ]);

        // Story 10
        DB::table('plots')->insert([
            [
                'id' => 125,
                'story_id' => 10,
                'plot_title' => 'Halaman 1',
                'page' => '1'
            ],
            [
                'id' => 126,
                'story_id' => 10,
                'plot_title' => 'Halaman 2',
                'page' => '2'
            ],
            [
                'id' => 127,
                'story_id' => 10,
                'plot_title' => 'Halaman 3',
                'page' => '3'
            ],
            [
                'id' => 128,
                'story_id' => 10,
                'plot_title' => 'Halaman 4',
                'page' => '4'
            ],
            [
                'id' => 129,
                'story_id' => 10,
                'plot_title' => 'Halaman 5',
                'page' => '5'
            ],
            [
                'id' => 130,
                'story_id' => 10,
                'plot_title' => 'Halaman 6',
                'page' => '6'
            ],
            [
                'id' => 131,
                'story_id' => 10,
                'plot_title' => 'Halaman 7',
                'page' => '7'
            ],
            [
                'id' => 132,
                'story_id' => 10,
                'plot_title' => 'Halaman 8',
                'page' => '8'
            ],
            [
                'id' => 133,
                'story_id' => 10,
                'plot_title' => 'Halaman 9',
                'page' => '9'
            ],
            [
                'id' => 134,
                'story_id' => 10,
                'plot_title' => 'Halaman 10',
                'page' => '10'
            ],
            [
                'id' => 135,
                'story_id' => 10,
                'plot_title' => 'Halaman 11',
                'page' => '11'
            ],
            [
                'id' => 136,
                'story_id' => 10,
                'plot_title' => 'Halaman 12',
                'page' => '12'
            ]
        ]);

        DB::table('plot_assets')->insert([
            [
                'plot_id' => 125,
                'filename' => 'katak_kecil_dan_ular_kecil_1.png'
            ],
            [
                'plot_id' => 126,
                'filename' => 'katak_kecil_dan_ular_kecil_2.png'
            ],
            [
                'plot_id' => 127,
                'filename' => 'katak_kecil_dan_ular_kecil_3.png'
            ],
            [
                'plot_id' => 128,
                'filename' => 'katak_kecil_dan_ular_kecil_4.png'
            ],
            [
                'plot_id' => 129,
                'filename' => 'katak_kecil_dan_ular_kecil_5.png'
            ],
            [
                'plot_id' => 130,
                'filename' => 'katak_kecil_dan_ular_kecil_6.png'
            ],
            [
                'plot_id' => 131,
                'filename' => 'katak_kecil_dan_ular_kecil_7.png'
            ],
            [
                'plot_id' => 132,
                'filename' => 'katak_kecil_dan_ular_kecil_8.png'
            ],
            [
                'plot_id' => 133,
                'filename' => 'katak_kecil_dan_ular_kecil_9.png'
            ],
            [
                'plot_id' => 134,
                'filename' => 'katak_kecil_dan_ular_kecil_10.png'
            ],
            [
                'plot_id' => 135,
                'filename' => 'katak_kecil_dan_ular_kecil_11.png'
            ],
            [
                'plot_id' => 136,
                'filename' => 'katak_kecil_dan_ular_kecil_12.png'
            ]
        ]);
    }
}
