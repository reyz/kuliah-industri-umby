<?php

use Illuminate\Database\Seeder;

class SongSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('songs')->insert([
            [
                'title' => 'Ampar-Ampar Pisang',
                'video' => 'ampar_ampar_pisang.mp4'
            ],
            [
                'title' => 'Apuse',
                'video' => 'apuse.mp4'
            ],
            [
                'title' => 'Bungong Jeumpa',
                'video' => 'bungong_jeumpa.mp4'
            ],
            [
                'title' => 'Cik Cik Periuk',
                'video' => 'cik_cik_periuk.mp4'
            ],
            [
                'title' => 'Cublak Cublak Suweng',
                'video' => 'cublak_cublak_suweng.mp4'
            ],
            [
                'title' => 'Gemu Famire',
                'video' => 'gemu_famire.mp4'
            ],
            [
                'title' => 'Ilir-Ilir',
                'video' => 'ilir_ilir.mp4'
            ],
            [
                'title' => 'Kicir-Kicir',
                'video' => 'kicir_kicir.mp4'
            ],
            [
                'title' => 'Mejangeran',
                'video' => 'mejangeran.mp4'
            ],
            [
                'title' => 'Pakarena',
                'video' => 'pakarena.mp4'
            ],
            [
                'title' => 'Rasa Sayange',
                'video' => 'rasa_sayange.mp4'
            ],
            [
                'title' => 'Rek Ayo Rek',
                'video' => 'rek_ayo_rek.mp4'
            ],
            [
                'title' => 'Sinanggar Tullo',
                'video' => 'sinanggar_tullo.mp4'
            ],
            [
                'title' => 'Soleram',
                'video' => 'soleram.mp4'
            ],
            [
                'title' => 'Tokecang',
                'video' => 'tokecang.mp4'
            ]
        ]);
    }
}
