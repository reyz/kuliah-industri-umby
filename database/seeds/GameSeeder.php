<?php

use Illuminate\Database\Seeder;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('games')->insert([
            [
                'title' => 'Boi Boinan',
                'philosophy' => 'Permainan yang biasanya dlakukan sepulang sekolah ataupun sore hari ini juga bisa melatih kecepatan fisik dan otak. Pasalnya setiap anggota harus fokus pada tugasnya. Anggota yang bertugas menjaga piramid harus cepat dalam menyusun piramid yang terkena lemparan bola. Mereka menggunakan otak untuk menemukan cara agar susunan itu rapi dan cepat selesai.',
                'how_to_play' => 'Boi-boian merupakan permainan tradisional yang dimainkan di suatu daerah di Indonesia. Kita sangat jarang menemukan permainan ini karena mungkin hanya kita lihat di daerah asalnya saja. Permainan ini dilakukan oleh lima hingga sepuluh orang. Cara memainkan permainan ini yaitu dengan menyusun satu lempengan batu. Lalu siapkan bola kecil yang dibuat dari kertas untuk melepar tumpukan batu. Kita gunakan kertas agar tidak sakit waktu melempar kertas tersebut. Setelah itu salah satu pemain melempar bola, jika tumpukan batu rubuh maka penjaga wajib mengambil bola dan dilempar ke pemain yang lainnya.',
                'picture' => 'boi_boinan.png',
            ],
            [
                'title' => 'Bola Bekel',
                'philosophy' => '',
                'how_to_play' => 'Ada beberapa benda yang perlu disiapkan untuk memainkannya, yang pertama bola bekel yang bentuknya bola kecil, biasanya di dalam bola diisi air. Dan ada enam biji yang disebut bekel. Permainan ini ada beberapa step, mulai dari step mengambil satu biji bekel sampai enam bekel. 
Lalu membalik biji bekel yang lain dan biji membentur lambang laguk , dan dimulai dari mengambil satu biji sampai enam biji. Lanjut lagi dengan membalik kearah sebaliknya dan sama prosesnya, mengambil dari satu biji sampai enam biji. Jika step sudah selesai, dilanjut dengan membalik lagi biji bekel yang ada tanda titiknya. Sama seperti yang tadi dilakukan secara berurutan mulai dari mengambil satu biji hingga enam biji. Setelah selesai semua step, maka jika ada yang berhasil memenangkan step terlebih dahulu maka dijadikan pemenang. Dan yang menang mendapatkan hadiah berupa menyuruh anak-anak menjadi patung atau memberi bedak di bagian wajah yang kalah.',
                'picture' => 'bola_bekel.jpg',
            ],
            [
                'title' => 'Congklak/Dakon',
                'philosophy' => 'Permainan anak tradisional tersebut kaya aspek pendidikannya. Banyak memberikan pengajaran bagi anak-anak. mulai dari nilai kerja sama, usaha, disiplin, ketelitian, kesetiakawanan sosial, serta sportivitas dan sebagainya, Dari berbagam makna filosfi yang ada pada seni permainan tradional ini Anda akan diajak untuk memahami bersabar, berpikir, dan ulet dalam proses hidup dan kehidupan dalam mencapai puncak akhir kehidupan yang di simbolkan dengan terpenuhinya lubang besar atas biji-biji congklaknya.',
                'how_to_play' => 'Menggunakan biji congklak yang terbuat dari cangkang karang tapi ada juga yang menggunakan batu, lalu menggunakan papan congklak yang berisi 16 lubang. Permainan ini hanya bisa dilakukan oleh dua orang saja. Biji congklak berisi 98 buah dan papan congklak ada yang terbuat dari plastik namun juga ada yang dari kayu. Awal memainkan permainan ini dengan suit menentukan siapa yang jalan dulan, lalu jika ada yang menang maka pemain harus mengambil semua biji dari salah satu lubang dan biji tersebut diisi satu persatu ke lubang yang sudah ditentukan, dari kiri atau kanan. Hingga biji habis dan setelah itu ambil lagi semua biji dari tempat terakhir biji diletakkan. Begitu seterusnya hingga siapa yang mendapat biji paling banyak maka ia yang menjadi pemenang.',
                'picture' => 'dakon.png',
            ],
            [
                'title' => 'Egrang',
                'philosophy' => 'Permainan yang menggunakan bambu sebagai pijakannya ini memang saat ini sudah sangat jarang ditemui, kecuali dalam festiva-festival tertentu. Rasa percaya diri sangat berpengaruh pada permainan ini, di saat percaya diri sudah tumbuh, otomatis keseimbangan tubuh juga akan terjaga. Makna yang terkandung dalam permainan ini adalah saat kehidupan memang sedang tidak berpihak pada kita, yakinlah bahwa dengan semangat dan percaya diri yang tinggi, semua itu akan bisa dilalui. Hasilnya, keseimbangan hidup pun akan terjamin.',
                'how_to_play' => 'Egrang merupakan dua tongkat yang panjang dan di bagian tengah diberikan pembatas. Setelah itu kita naik diatas pijakan yang sudah diberikan. Jika jatuh maka akan diberi hukuman. Tetapi untuk awal-awal kita tidak perlu membuat hukuman karena masih belajar, tapi jika sudah bisa menggunakan maka harus diberi hukuman.',
                'picture' => 'egrang.png',
            ],
            [
                'title' => 'Engklek',
                'philosophy' => 'Filosofi yang terkandung dalam permainan ini merupakan sebuah simbol dalam mencapai kekuasaan atau memiliki tempat tinggal. Hal itu tentu tidak mudah dicapai. Seseorang harus tangguh dalam menjalani segala permasalahan kehidupan yang ada.',
                'how_to_play' => 'Engklek dimainkan oleh anak laki-laki dan juga perempuan. Bisa dilakukan oleh dua orang saja dan maksimal lima orang, sebab untuk memainkannya harus menunggu giliran dan jika banyak yang bermain maka akan lama menunggunya. Cara bermainnya dengan menggambar kotak-kotak di latar. Bermainnya dilapangan yang terang agar mudah menggambar kotak-kotaknya. Ada sembilan kotak yang terdiri dari tiga buah kotak horizontal, lalu disambung tiga kotak vertikal, setelah itu tambah satu kotak diatasnya dan terakhir dua kotak dihorizontal. Satu persatu pemain melompati kotak tersebut dari awal hingga terakhir. Melompatnya harus menggunakan satu kaki, jika kaki terjatuh maka harus menaruh batu disalah satu kotak terakhir sebagai tanda untuk mengawali giliran.',
                'picture' => 'engklek.jpg',
            ],
            [
                'title' => 'Gangsing',
                'philosophy' => 'Filosofi dari gasing sendiri kenapa bisa lama karena seimbang. Kalau manusia seimbang antara jasmani dan rohani, dia akan akan panjang umur. Manusia itu hidup selalu berputar, selalu bergerak, itu melatih kreativitas, bagaimana menjalani hidup harus bisa bergerak.',
                'how_to_play' => 'Bentuknya seperti bola yang ditengahnya terdapat tali yang dililitkan dan diikat pusatnya. Jika tali dilempar dengan benar maka gasing akan memutar seimbang. Permainan ini tidak sulit namun butuh kekuatan untuk melemparnya, karena jika ragu untuk melempar maka gasing bisa cepat jatuh.',
                'picture' => 'gangsing.png',
            ],
            [
                'title' => 'Gobak Sodor',
                'philosophy' => 'Selain kebersamaan, kita juga bisa belajar kerja sama yang kompak antara satu penjaga dan penjaga lain agar lawan tidak lepas kendali untuk keluar dari kungkungan kita. Di pihak lain bagi penerobos yang piawai, disana masih banyak pintu-pintu yang terbuka apabila satu celah dirasa telah tertutup. Jangan putus asa apabila dirasa ada pintu satu yang dijaga, karena masih ada pintu lain yang siap menerima kedatangan kita, yang penting kita mau mau berusaha dan bertindak segera. Ingatlah bahwa peluang selalu ada, walaupun terkadang nilai probabilitasnya sedikit.',
                'how_to_play' => 'Permainan ini disebut benteng sodor atau gobak sodor, karena ada beberapa kelompok yang menjaga benteng mereka. Satu kelompok terdiri dari minimal 2 orang. Dimulai dari hompimpa dan dilihat mana yang menjadi pemenang. Setelah hompimpa selesai, maka yang menjadi pemenang boleh memulai duluan, lari dan mengejar ke arah benteng lawan. Tapi permainan ini harus cepat larinya, jika tidak cepat akan kena lawan.',
                'picture' => 'gobak_sodor.png',
            ],
            [
                'title' => 'Kelereng',
                'philosophy' => 'Permainan kelereng dapat mengatur emosi (relaks), melatih kemampuan motorik, melatih kemampuan berpikir (kognitif), kemampuan berkompetensi, kemampuan sosial (menjalin pertemanan), bersikap jujur, serta melatih taraf kecermatan dan ketelitian.',
                'how_to_play' => 'Gundu meupakan kelereng yang bentuknya seperti kaca bening dan biasanya yang memainkan ini adalah anak laki-laki. Untuk memainkan permainan ini cukup mudah karena hanya menyentil kelereng yang kita punya dan harus mengenai kelereng lawan. Jika ada beberapa gundu yang kena dengan gundu kita, maka gundu lawan akan menjadi milik kita. Permainan ini bisa dilakukan oleh dua orang sampai tujuh orang.',
                'picture' => 'kelereng.png',
            ],
            [
                'title' => 'Lompat Tali',
                'philosophy' => 'Permainan lompat tali mungkin banyak digunakan oleh perempuan, namun tak jarang anak-anak laki juga suka gabung ikutan main. Permainan melompat tali yang terbuat dari karet ini memang memiliki tinggi rintangan yang semakin tinggi. Makna dari permainan ini adalah ada banyaknya rintangan yang menghadang dalam kehidupan. Semakin dewasa, semakin besar juga rintangannya. Hingga pada saatnya “merdeka”, kita dapat terbebas dari segala macam rintangan. Yang dimaksud dengan merdeka adalah kebahagiaan di dunia dan di akhirat. Ternyata memang sedikit atau banyak output dari ragam permainan tradisional itu memang berpengaruh pada kita. Penciptaan sebuah permainan memang ternyata harus dipikirkan dengan matang. Harus mengandung makna dan nilai yang dalam supaya bisa diimplementasikan di kemudian hari.',
                'how_to_play' => 'Lompat tali ini bisa menggunakan karet atau tali tambang. Kita bisa membuat tali sendiri dengan karet, caranya menyambungkan satu persatu karet hingga panjang dan setelah itu diikat ujungnya dan karet tersebut bisa kita gunakan untuk memainkan lompat tali. Permainan ini bisa dilakukan dua orang hingga lebih dari sepuluh. Ada dua orang yang memegang tali agar tidak putus. Tapi jika tidak ingin memegang tali maka kita bisa mengikatkan tali dengan pohon atau apapun yang bisa untuk memegang tali ataupun karet. Cara bermainnya dimulai dari tali di letakkan paling bawah lalu kita melompat. Jika tidak bisa melompat maka kita harus menunggu giliran terakhir dan mengulang lompatan dari awal. Setelah itu tali diarahkan sampai atas kepala, dan kita harus melompat diatas tali. Yang menang boleh menyuruh yang kalah untuk melakukan apa saja tetapi tidak boleh yang berat dan aneh-aneh.',
                'picture' => 'lompat_tali.png',
            ],
            [
                'title' => 'Petak Umpet',
                'philosophy' => 'Siapa yang sangka kalau permainan ini hanyalah simbol mengenai kehidupan di dunia dan setelah kematian. Orang-orang yang sedang bermain bisa direpresentasikan dengan orang yang hidup di dunia. Saat mereka ditemukan, itu tandanya mereka sudah dipanggil oleh Tuhan untuk berpulang. Hingga sebelum semuanya dipanggil Tuhan, mereka menyaksikan kehidupan di dunia.',
                'how_to_play' => 'Pertama ada permainan yang bernama petak umpet, permainan ini dilakukan oleh lebih dari dua orang. Caranya sangat mudah sekali. Ada satu orang yang menjadi penjaga dan mencari temannya yang menghilang, sedangkan orang yang lain mengumpet disuatu tempat. Misalnya bermain dengan tujuh orang, lalu dimulai dengan hompimpa untuk menentukan siapa yang menjadi penjaga. Jika hompimpa sisa satu orang maka langsung dinyatakan dia kalah, tetapi jika hompimpa berbanding 3:4, maka yang tiga orang melakukan hompimpa hingga sisa satu orang yang kalah. Orang yang kalah tersebut dinamakan kucing. Permainan dimulai dengan hingga kucing yang menjaga dan harus menutup matanya lalu menghitung dari satu sampai sepuluh. Orang-orang yang lainnya harus mengumpet di belakang pohon atau dibawah pohon, jangan sampai si kucing menemukan. Jika si kucing melengah maka orang-orang yang lain harus segera lari menuju tempat penjaga si kucing tadi, dan berteriak inglo. Jika sudah ada yang teriak inglo, maka orang tersebut menjadi pemenang dan kucing tetap mencari orang-orang lain yang masih belum ditemukan. Si kucing terus mencari dan jika menemukan orang yang mengumpet, maka orang tersebut menjadi kalah dan kucing digantikan dengan orang yang kalah tadi. Begitulah permainan tersebut dilakukan hingga permainan berakhir. Waktu permainan bisa dilakukan pada pagi hingga siang hari. Jika sore hari biasanya anak-anak tidak boleh keluar rumah apalagi jika menjelang magh Permainan ini sangat digemari dari jaman dahulu sampai saat ini, tapi sekarang hanya beberapa anak saja yang bermain permainan petak umpet, karena sekarang anak-anak hanya sibuk dengan gadgetya dan lupa dengan teman-temannya.',
                'picture' => 'petak_umpet.jpg',
            ],
            [
                'title' => 'Pletokan',
                'philosophy' => 'Permainan ini mencerminkan peperangan yang mana zaman dahulu yang belum menggunakan senjata modern seperti pistol. Permainan ini mengajarkan bahwa pentingnya bekerja sama dan mengatur strategi untuk memenangkan pertandingan.',
                'how_to_play' => 'Permainan ini terbuat dari bambu yang kuat agar tidak gampang pecah. Setelah itu bambu dibagi menjadi dua kemudian buat peluru menggunakan kertas yang dilitikan dan dibuat seperti bola. Kemudian tambahkan daun pandan agar suaranya menjadi nyaring. Seteah semua bahan tersedia, lakukan penembakan ke arah lahan kosong.',
                'picture' => 'pletokan.png',
            ],
            [
                'title' => 'Ular Naga',
                'philosophy' => 'Dibalik Syair lagu ular naga terdapat bait-bait yang mengandung ritual pesembahan atau pengorbanan"Ular naga panjangnya bukan kepalang, menjalar - jalar selalu kian kemari, umpan yang lezat itulah yang dicari, ini dianya yang terbelakang" coba anda baca bait pertama, disana terdapat pemujaan yang memuji muji ular naga, lalu pada bait terakhir baru menyerahkan pengorbanan. Pada awal permaianan beberapa anak mengajukan menjadi Induk dan Gerbang, Tetapi tahukah anda, tujuan menjadi induk dan gerbang. induk yang dimaksut menurut mitos adalah seorang tertua/kepala adat/kepala suku sedangkan gerbang itu terdapat sebuah simbol yang mana dulu merupakan gerbang goa yang di dalamnya terdapat sang naga. Ketika lagu habis dinyanyikan, anak yang berdiri paling belakang akan di tangkap oleh gerbang, menurut mitos ini bermaksut korban atau persembahan pada jaman dahulu itu di lemparkan ke dalam goa untuk menjadi santapan naga. Permainan akan dimulai kembali. Dengan terdengarnya nyanyi, Ular Naga kembali bergerak dan menerobos Gerbang, dan lalu ada lagi seorang anak yang ditangkap. Perbantahan lagi. Demikian berlangsung terus, hingga Induk akan kehabisan anak dan permainan selesai.',
                'how_to_play' => 'Permainan selanjutnya ada ular naga. Pada zaman dahulu permainan ini sangat digemari oleh anak-anak umur lima sampai dua belas tahun. Permainan ini lebih baik dilakukan di lapangan, karena semakin banyak pemain akan semakin seru. Biasanya permainan ini dilakukan lebih dari tujuh orang. Cara bermainnya dengan menentukan siapa yang menjadi penjaga dua orang dan sisanya berjalan melewati penjaga. Untuk memilih penjaga, harus melakukan hompimpa agar lebih adil. Setelah ditentukan yang menjadi penjaga, maka sisa orangnya berbaris dengan tangan ditaruh dipundak teman depannya, lalu berjalan melingkar melewati penjaga. Sambil berjalan menyanyikan lagu ular naga panjangnya, hingga selesai. Jika nyanyian sudah selesai maka penjaga menangkap satu orang dan orang yang tertangkap harus keluar dari barisan.',
                'picture' => 'ular_naga.png',
            ]
        ]);
    }
}
