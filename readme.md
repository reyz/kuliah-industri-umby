## KI UMBY

Aplikasi ini digunakan untuk mata kuliah Kuliah Industri untuk tahun ajaran 2019. Member:

1. ABDURRAHMAN ARGHA PUTRA
2. ARIO BIJAKSANA
3. FEBRI RAHMADSYAH
4. PRASTOTO SANDI NUGROHO
5. ROES WIBOWO

## Cara Instalasi

1. Download git dan composer.
2. Buat database MySQL baru.
3. Copy `.env.example` menjadi `.env`
4. Update database connection.
5. Run: `composer install`
6. Run: `php artisan doctrine:migrations:migrate`
7. Run: `php artisan db:seed`
8. Extract file di: `public/assets/uploads/extract-here.zip`
9. Development run: `php artisan serve`

## License

Aplikasi ini dibuat dengan framework [Laravel](https://laravel.com) dan di bawah lisensi [MIT license](https://opensource.org/licenses/MIT).
