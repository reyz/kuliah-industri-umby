@extends('layouts.backend')

@section('title')
    Login
@endsection

@section('stylesheet')
    <link rel="stylesheet" href="{{ url('/assets/modules/bootstrap-social/bootstrap-social.css') }}">
@endsection

@section('body')
<section class="section">
    <div class="container mt-5">
        <div class="row">
            <div
                    class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                <div class="login-brand">
                    <img src="{{ url('/assets/img/stisla-fill.svg') }}" alt="logo" width="100"
                         class="shadow-light rounded-circle">
                </div>

                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Login</h4>
                    </div>

                    <div class="card-body">
                        <form method="POST">
                            @if(session('message'))
                                <div class="alert alert-danger" role="alert">
                                    <span class="help-block text-error"><i class="fa fa-info-circle"></i> {{ session('message') }}</span>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="username">Username</label>
                                <input id="username" type="text"
                                       class="form-control @if($errors->get('username')) is-invalid @endif"
                                       name="username"
                                       value="{{ request()->old('username')  }}"
                                       tabindex="1" required autofocus>
                                <div class="invalid-feedback">
                                    {!! implode('<br>', $errors->get('email')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="d-block">
                                    <label for="password" class="control-label">Password</label>
                                </div>
                                <input id="password" type="password"
                                       class="form-control @if($errors->get('password')) is-invalid @endif"
                                       name="password" tabindex="2" required>
                                <div class="invalid-feedback">
                                    {!! implode('<br>', $errors->get('password')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">Login</button>
                            </div>
                        </form>
                    </div>
                    <div class="simple-footer">
                        Copyright &copy; {{ date('Y') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
