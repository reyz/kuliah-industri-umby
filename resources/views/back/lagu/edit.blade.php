@extends('layouts.backend')

@section('title', 'Membuat Lagu Daerah Baru')
@section('stylesheet')
    <link rel="stylesheet"
          href="{{ url('/assets/modules/summernote/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/css/customs.css') }}">
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
        @include('layouts.back._partials.sidebar')
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <a href="{{ url(route('back.lagu-index')) }}"
                       class="btn btn-info"
                       style="margin-right: 25px">
                        <i class="fas fa-chevron-left"></i>
                        Kembali
                    </a>
                    <h1>Edit Lagu Daerah</h1>
                </div>
                <div class="section-body">
                    @if(!$errors->isEmpty())
                        <div class="alert alert-danger alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>&times;</span>
                                </button>
                                @foreach($errors->all() as $error)
                                    <i class="fa fa-info-circle"></i>{{ $error }}<br/>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <form method="POST"
                          action="{{ url(route('back.lagu-edit', [$lagu->getId()])) }}"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <div class="form-group">
                                    <label for="judul">Judul</label>
                                    <input id="judul"
                                           class="form-control col-md-6 col-sm-12 {!! empty($errors->first('title')) ? '' : 'is-invalid' !!}"
                                           name="title" type="text"
                                           value="{{ request()->old('title') ? request()->old('title') : $lagu->getTitle() }}">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('title') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="deskripsi-content-content">Deskripsi</label>
                                    <textarea class="summernote" id="deskripsi-content"
                                              name="deskripsi">{{request()->old('deskripsi') ? request()->old('deskripsi') : $lagu->getDeskripsi()}}</textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success" type="submit">Submit
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group">
                                    <div class="section-title">Audio</div>
                                    <div class="audio-preview" id="audio-preview">
                                        <label for="audio-upload" id="audio-label">Pilih
                                            File Audio</label>
                                        <input accept="audio/*" id="audio-upload"
                                               name="audio" type="file">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection

@section('scripts')
    <script src="{{ url('/assets/modules/summernote/summernote-bs4.js') }}"></script>
    <script src="{{ url('/assets/modules/upload-preview/assets/js/jquery.uploadPreview.js') }}"></script>
    <script>
      $(document).ready(() => {
        $.uploadPreview({
          input_field: '#audio-upload',
          preview_box: '#audio-preview',
          label_field: '#audio-label',
          label_default: 'Choose File',
          label_selected: 'Change File',
          no_label: false,
          success_callback: null,
        });
      });
    </script>
@endsection