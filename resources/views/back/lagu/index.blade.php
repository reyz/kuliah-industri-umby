@extends('layouts.backend')

@section('title')
    Daftar Lagu Daerah
@endsection

@section('stylesheet')
    @if(count($lagu))
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/datatables.min.css') }}">
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
    @endif
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
    @include('layouts.back._partials.sidebar')
    <!-- Main Content -->
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>Daftar Lagu Daerah</h1>
                </div>
                <div class="section-body">
                    @if(session('notification') ?? false)
                        <div class="alert alert-primary alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>×</span>
                                </button>
                                {{ session('notification') }}
                            </div>
                        </div>
                    @endif

                    <a href="{{ url(route('back.lagu-add')) }}" class="btn btn-info"
                       style="margin-bottom: 25px">Tambah Lagu Daerah</a>
                    <div class="clearfix"></div>
                    <table class="table table-hover" id="data-table">
                        <thead>
                        <tr>
                            <th scope="col" style="width: 5%">No</th>
                            <th scope="col" style="width: 60%">Judul</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($lagu))
                            <?php /** @var \App\Entities\Songs $page */ ?>
                            @foreach($lagu as $index => $lg)
                                <tr>
                                    <th scope="row">{{ $index + 1 }}</th>
                                    <td>{{ $lg->getTitle() }}</td>
                                    <td class="text-center">
                                        <a class="btn btn-icon btn-info "
                                           href="#" id="view-lagu">
                                            <i class="far fa-eye"></i> Lihat
                                        </a>
                                        <a class="btn btn-icon btn-warning "
                                           href="{{ url(route('back.lagu-edit', [$lg->getId()])) }}">
                                            <i class="far fa-edit"></i> Ubah
                                        </a>
                                        <a class="btn btn-icon btn-danger "
                                           href="{{ url(route('back.lagu-delete', [$lg->getId()])) }}">
                                            <i class="far fa-times"></i> Hapus
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">
                                    <div class="empty-state" data-height="400"
                                         style="height: 400px;">
                                        <div class="empty-state-icon">
                                            <i class="fas fa-question"></i>
                                        </div>
                                        <h2>No record found!</h2>
                                        <p class="lead">
                                            Tidak ada data permainan tradisional
                                            ditemukan.
                                        </p>
                                    </div>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </section>
            <a href="#" id="modal" style="display: none">a</a>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection

@section('scripts')
    @if(count($lagu))
        <script src="{{ url('/assets/modules/datatables/datatables.js') }}"></script>
        <script src="{{ url('/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ url('/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>

        <script>
          $(document).ready(() => {
            const modalSelector = $('#modal');
            let modalBody = '';
            let data = '';

            $('#view-lagu').on('click', function(e) {
              e.preventDefault();
              $.ajax({
                method: 'GET',
                url: "{{ url(route('back.lagu-view', [$lg->getId()])) }}",
                error: function() {
                  alert('Terjadi kesalahan, silahkan hubungi web administrator');
                },
              }).then((res) => {
                data = JSON.parse(res);

                modalSelector.click();
              });
            });

            modalBody = `${data.deskripsi}\n\n
                        <audio controls autoplay preload="auto" style="width: 100%" >
                            <source src="{{ url('assets/uploads') }}/${data.audio}" type="${data.type}">
                            Your browser does not support the audio element.
                        </audio>`;

            modalSelector.fireModal(
                {
                  title: data.title,
                  body: modalBody,
                  center: true,
                });

            $('#data-table').dataTable({
              'columnDefs': [
                {
                  'sortable': false,
                  'targets': [2],
                },
              ],
            });

          });
        </script>
    @endif
@endsection
