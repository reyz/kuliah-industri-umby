@extends('layouts.backend')

@section('title')
    Ranking Konten
@endsection

@section('stylesheet')
    <link rel="stylesheet"
          href="{{ url('/assets/modules/datatables/datatables.min.css') }}">
    <link rel="stylesheet"
          href="{{ url('/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet"
          href="{{ url('/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
    <link rel="stylesheet"
          href="{{ url('/assets/modules/bootstrap-daterangepicker/daterangepicker.css') }}">

@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
    @include('layouts.back._partials.sidebar')
    <!-- Main Content -->
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>Daftar View Logs</h1>
                </div>
                <div class="alert alert-warning alert-has-icon">
                    <div class="alert-icon"><i class="fas fa-trophy"></i></div>
                    <div class="alert-body">
                        <div class="alert-title">Konten Terpopuler</div>
                        <span></span>
                    </div>
                </div>
                <div class="section-body">
                    <a href="javascript:"
                       class="btn btn-primary daterange-btn icon-left btn-icon mb-4"><i
                                class="fas fa-calendar"></i> Choose Date <span
                                id="daterange-btn"></span>
                    </a>
                    <div class="clearfix"></div>
                    <table class="table table-hover" id="data-table">
                        <thead>
                        <tr>
                            <th scope="col" style="width: 15%">Rank No.</th>
                            <th scope="col" style="width: 35%">Kategori</th>
                            <th scope="col" style="width: 35%">Kontent</th>
                            <th scope="col">Jumlah dibaca</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </section>
            <a href="#" id="modal" style="display: none">a</a>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection

@section('scripts')
    <script src="{{ url('/assets/modules/datatables/datatables.js') }}"></script>
    <script src="{{ url('/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ url('/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
    <script src="{{ url('/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
    <script src="{{ url('/assets/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <script defer="defer">
      fetchData('no');

      function fetchData(isDateSearch = false, startDate = '', endDate = '') {
        $('#data-table').DataTable({
          'processing': true,
          'serverSide': true,
          'ordering': false,
          'searching': false,
          'lengthChange': false,
          'order': [],
          'ajax': {
            url: '{{ url(route('back.ranking-data')) }}',
            type: 'POST',
            dataType: 'json',
            data: {
              _token: "{{csrf_token()}}",
              isDateSearch: isDateSearch,
              startDate: startDate,
              endDate: endDate,
            },
          },
          'initComplete': function(settings, json) {
            if (json.recordsTotal > 0) {
              $('.alert-title').
                  html('Kategori Paling Banyak Di Baca');

              if (json.startDate != null) {
                  $('.alert-body > span').
                  html('<span>Menampilkan data untuk ' + json.startDate +
                      ' sampai dengan ' +
                      json.endDate + '</span>');
              } else {
                  $('.alert-body > span').
                  html('<span>Menampilkan data untuk sepanjang masa.</span>');
              }
            } else {
              $('.alert-title').
                  html('Tidak ada data');
              $('.alert-body > span').
                  html('<span></span>');
            }
          },
          'columns': [
            {'data': 'no'},
            {'data': 'category'},
            {'data': 'content'},
            {'data': 'count'},
          ],
        });
      }

      $('.daterange-btn').daterangepicker({
        ranges: {
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()]
        },
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
      }, function(start, end) {
        const startDate = start.format('YYYY-MM-D hh:mm:ss');
        const endDate = end.format('YYYY-MM-D hh:mm:ss');

        $('#daterange-btn').
            html(start.format('D/MMMM/YYYY') + ' - ' + end.format('D/MMMM/YYYY'));

        $('#data-table').DataTable().destroy();
        fetchData('yes', startDate, endDate);
      });
    </script>
@endsection
