@extends('layouts.backend')

@section('title', 'Edit Lagu Daerah')
@section('stylesheet')
    <link rel="stylesheet"
          href="{{ url('/assets/modules/summernote/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/css/customs.css') }}">
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
        @include('layouts.back._partials.sidebar')
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <a href="{{ url(route('back.song-index')) }}"
                       class="btn btn-info"
                       style="margin-right: 25px">
                        <i class="fas fa-chevron-left"></i>
                        Kembali
                    </a>
                    <h1>Edit Lagu Daerah</h1>
                </div>
                <div class="section-body">
                    @if(!$errors->isEmpty())
                        <div class="alert alert-danger alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>&times;</span>
                                </button>
                                @foreach($errors->all() as $error)
                                    <i class="fa fa-info-circle"></i>{{ $error }}<br/>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <form method="POST"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <div class="form-group">
                                    <label for="judul">Judul</label>
                                    <input id="judul"
                                           class="form-control col-md-6 col-sm-12 {!! empty($errors->first('title')) ? '' : 'is-invalid' !!}"
                                           name="title" type="text"
                                           value="{{ request()->old('title') ? request()->old('title') : $songs->getTitle() }}">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('title') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="deskripsi-content-content">Deskripsi</label>
                                    <textarea class="summernote" id="deskripsi-content"
                                              name="description">{{request()->old('description') ? request()->old('description') : $songs->getDescription()}}</textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success" type="submit">Submit
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group">
                                    <div class="section-title">Video</div>
                                    <div class="video-preview" id="video-preview">
                                        <label for="video-upload" id="video-label">Pilih
                                            File Video</label>
                                        <video controls width="294px" height="250px">
                                            {{-- FIXME: Can't play on Safari, because bug on Safari --}}
                                            <source src="{{ url('/assets/uploads/'.$songs->getVideo()) }}"
                                                    id="video_here"/>
                                        </video>
                                        <input accept="video/*, video/mp4, video/x-m4v"
                                               id="video-upload"
                                               name="video" type="file">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection

@section('scripts')
    <script src="{{ url('/assets/modules/summernote/summernote-bs4.js') }}"></script>
    <script>
      $(document).ready(() => {
        $('.summernote').summernote({
          height: 300,
          toolbar: [
            ['style', ['stye']],
            ['font', ['bold', 'italic', 'underline']],
            ['fontname', ['fontame']],
            ['color', ['color']],
            ['height', ['height']],
            ['para', ['ul', 'ol', 'paragrahp']],
          ],
        });

        const source = $('#video_here');

        $('#video-upload').on('change', function() {
          source[0].src = URL.createObjectURL(this.files[0]);

          source.parent()[0].load();
          // $('#video-label').remove();
        });
      });
    </script>
@endsection