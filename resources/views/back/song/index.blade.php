@extends('layouts.backend')

@section('title')
    Daftar Lagu Daerah
@endsection

@section('stylesheet')
    @if(count($songs))
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/datatables.min.css') }}">
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
    @endif
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
    @include('layouts.back._partials.sidebar')
    <!-- Main Content -->
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>Daftar Lagu Daerah</h1>
                </div>
                <div class="section-body">
                    @if(session('notification') || session('error_notification') ?? false)
                        <div class="alert {{ session('notification') ? 'alert-success' : 'alert-danger' }} alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>×</span>
                                </button>
                                {{ session('notification') }}
                                {{ session('error_notification') }}
                            </div>
                        </div>
                    @endif

                    <a href="{{ url(route('back.song-add')) }}" class="btn btn-info"
                       style="margin-bottom: 25px">Tambah Lagu Daerah</a>
                    <div class="clearfix"></div>
                    <table class="table table-hover" id="data-table">
                        <thead>
                        <tr>
                            <th scope="col" style="width: 5%">No</th>
                            <th scope="col" style="width: 60%">Judul</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($songs))
                            @foreach($songs as $index => $song)
                                <tr>
                                    <th scope="row">{{ $index + 1 }}</th>
                                    <td>{{ $song->getTitle() }}</td>
                                    <td class="text-center">
                                        <a class="btn btn-icon btn-info "
                                           href="#" id="view-lagu">
                                            <i class="far fa-eye"></i> Lihat
                                        </a>
                                        <a class="btn btn-icon btn-warning "
                                           href="{{ url(route('back.song-edit', [$song->getId()])) }}">
                                            <i class="far fa-edit"></i> Ubah
                                        </a>
                                        <a class="btn btn-icon btn-danger "
                                           href="{{ url(route('back.song-delete', [$song->getId()])) }}">
                                            <i class="fas fa-times"></i> Hapus
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">
                                    <div class="empty-state" data-height="400"
                                         style="height: 400px;">
                                        <div class="empty-state-icon">
                                            <i class="fas fa-question"></i>
                                        </div>
                                        <h2>No record found!</h2>
                                        <p class="lead">
                                            Tidak ada data permainan tradisional
                                            ditemukan.
                                        </p>
                                    </div>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </section>
            <a href="#" id="modal" style="display: none">a</a>
        </div>
        {{-- Display Modal --}}
        <div class="modal fade" tabindex="-1" role="dialog" id="view-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-description"></div>
                        <video controls width="100%" height="100%">
                            <source src="" id="video_here"/>
                        </video>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection

@section('scripts')
    @if(count($songs))
        <script src="{{ url('/assets/modules/datatables/datatables.js') }}"></script>
        <script src="{{ url('/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ url('/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>

        <script>
          $(document).ready(() => {
            $('#view-lagu').on('click', function(e) {
              e.preventDefault();
              const video = $('#video_here');
              $.ajax({
                url: "{{ route('back.song-view', [$song->getId()]) }}",
                method: 'GET',
                success: (res) => {
                  const data = JSON.parse(res);

                  const modal = $('#view-modal');

                  $('.modal-title').text(data.title);
                  $('.modal-description').html(data.description);
                  video[0].src = `{{ url('/assets/uploads') }}/${data.video}`;
                  video.parent()[0].load();
                  modal.modal('show');
                },
              });

            });

            $('#view-modal').on('hide.bs.modal', function(e) {
              $('#video_here').trigger('pause');
            });

            $('#data-table').dataTable({
              'columnDefs': [
                {
                  'sortable': false,
                  'targets': [2],
                },
              ],
            });

          });
        </script>
    @endif
@endsection
