@extends('layouts.backend')

@section('title')
    Stories
@endsection

@section('stylesheet')

@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
        @include('layouts.back._partials.sidebar')
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>Plots</h1>
                </div>
                <div class="section-body">
                    @if(session('notification') ?? false)
                        <div class="alert alert-success alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>&times;</span>
                                </button>
                                {{ session('notification') }}
                            </div>
                        </div>
                    @endif
                    @if(session('error_notification') ?? false)
                        <div class="alert alert-danger alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>&times;</span>
                                </button>
                                {{ session('error_notification') }}
                            </div>
                        </div>
                    @endif
                    <a href="{{ url(route('back.plot-add', [$stories->getId()])) }}"
                       class="btn btn-info"
                       style="margin-bottom: 25px">Create
                        Plots</a>
                    <div class="clearfix"></div>
                    <div class="list-group" id="plots-list">
                        @forelse($plots as $index => $plot)
                            <div
                                    class="list-group-item list-group-item-action flex-column align-items-start"
                                    data-url="{{ url(route('back.plot-update-page', [$stories->getId(), $plot->getId()])) }}"
                            >
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">{{ $plot->getPlotTitle() }}</h5>
                                    <div>
                                        <a href="{{ url(route('back.plot-edit', [$stories->getId(), $plot->getId()])) }}"
                                           class="btn btn-icon btn-warning"
                                           data-url=""
                                        ><i
                                                    class="fas fa-edit"></i> Ubah</a>
                                        <a href="{{ url(route('back.plot-delete', [$stories->getId(), $plot->getId()])) }}"
                                           class="btn btn-icon btn-danger"><i
                                                    class="fas fa-times"></i> Hapus</a>
                                    </div>
                                </div>
                                <p class="mb-1">{{ strip_tags(htmlspecialchars_decode($plot->getContent())) }}</p>
                            </div>
                        @empty
                            <div class="empty-state" data-height="400"
                                 style="height: 400px;">
                                <div class="empty-state-icon">
                                    <i class="fas fa-question"></i>
                                </div>
                                <h2>Kami tidak dapat menemukan plot cerita apapun</h2>
                                <p class="lead">
                                    Untuk menyingkirkan pesan ini, silahkan isi
                                    dengan
                                    setidaknya 1 plot.
                                </p>
                                <a class="btn btn-primary mt-4"
                                   href={{ url(route('back.plot-add', [$stories->getId()])) }}>Buat
                                    Plot</a>
                            </div>
                        @endforelse
                    </div>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection

@section('scripts')
    <script src="{{ url('/assets/modules/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ url('/assets/modules/Sortable.min.js') }}"></script>
    <script>
      $(document).ready(function() {
        const el = document.getElementById('plots-list');
        new Sortable(el, {
          animation: 150,
          easing: 'cubic-bezier(1, 0, 0, 1)',
          onEnd: (e) => {
            const url = e.item.attributes[1].value;
            // +1 because index e start from 0(zero)
            const newPage = e.newIndex + 1;
            const oldPage = e.oldIndex + 1;

            $.ajax({
              url,
              data: {newPage, oldPage},
              method: 'POST',
              success: (e) => {
                console.log('suks', e);
              },
              error: () => {
                console.log('asdas');
              },
            });
          },
        });

        const deleteButton = $('.btn-danger');
        deleteButton.on('click', (e) => {
          e.preventDefault();
          const target = e.currentTarget.attributes;
          const HREF = target.href.value;

          swal({
            title: 'Yakin?',
            text: 'Setelah dihapus, anda tidak dapat mengembalikan cerita ini lagi!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
          }).then((res) => {
            if (res) {
              location.replace(HREF);
            } else {
              swal('Yey, Ceritanya tidak jadi dihapus');
            }
          });
        });
      });
    </script>
@endsection