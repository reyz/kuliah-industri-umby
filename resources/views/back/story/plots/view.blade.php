@extends('layouts.backend')

@section('title')
    View Plots - {{ $story->getTitle() }}
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
        @include('layouts.back._partials.sidebar')
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <a href="{{ url(route('back.story-index')) }}"
                       class="btn btn-info"
                       style="margin-right: 25px">
                        <i class="fas fa-chevron-left"></i>
                        Kembali
                    </a>
                    <h1>Preview {{ $story->getTitle() }}</h1>
                </div>
                <div class="section-body">
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-12">
                            <h4>Judul Cerita</h4>
                            <p>{{ $story->getTitle() }}</p>
                        </div>
                        <div class="col-6">
                            <div id="accordion">
                                <div class="accordion">
                                    <div class="accordion-header" role="button"
                                         data-toggle="collapse"
                                         data-target="#panel-body-1" aria-expanded="true">
                                        <h4>Plot 1</h4>
                                    </div>
                                    <div class="accordion-body collapse show"
                                         id="panel-body-1" data-parent="#accordion">
                                        <p class="mb-0">Ini isinya bro</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection
