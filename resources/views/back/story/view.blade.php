@extends('layouts.backend')

@section('title')
    Lihat Cerita - {{ $story->getTitle() }}
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
        @include('layouts.back._partials.sidebar')
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <a href="{{ url(route('back.story-index')) }}"
                       class="btn btn-info"
                       style="margin-right: 25px">
                        <i class="fas fa-chevron-left"></i>
                        Kembali
                    </a>
                    <h1>Preview Cerita - {{ $story->getTitle() }}</h1>
                </div>
                <div class="section-body">
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12">
                            <div id="accordion">
                                @forelse($stories as $story)
                                    <div class="accordion">
                                        <div class="accordion-header collapsed"
                                             role="button"
                                             data-toggle="collapse"
                                             data-target="#panel-body-{{ $story->plot->isPage() }}"
                                             aria-expanded="false">
                                            <h4>Halaman {{ $story->plot->isPage() }}
                                                - {{ $story->plot->getPlotTitle() }}</h4>
                                        </div>
                                        <div class="accordion-body collapse"
                                             id="panel-body-{{ $story->plot->isPage() }}"
                                             data-parent="#accordion">
                                            <img src="{{ url('/assets/uploads/'. $story->assets->getFilename()) }}"
                                                 class="img-fluid">
                                            <p class="mb-0">{!! htmlspecialchars_decode($story->plot->getContent())  !!}</p>
                                        </div>
                                    </div>
                                @empty
                                    <div class="empty-state" data-height="400"
                                         style="height: 400px;">
                                        <div class="empty-state-icon">
                                            <i class="fas fa-question"></i>
                                        </div>
                                        <h2>Kami tidak dapat menemukan plot cerita
                                            apapun</h2>
                                        <p class="lead">
                                            Untuk menyingkirkan pesan ini, silahkan isi
                                            dengan
                                            setidaknya 1 plot.
                                        </p>
                                        <a class="btn btn-primary mt-4"
                                           href={{ url(route('back.plot-add', [$story->getId()])) }}>Buat
                                            Plot</a>
                                    </div>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection
