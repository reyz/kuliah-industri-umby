@extends('layouts.backend')

@section('title')
    Stories
@endsection

@section('stylesheet')
    @if(count($stories))
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/datatables.min.css') }}">
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
    @endif
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
        @include('layouts.back._partials.sidebar')
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>Stories</h1>
                </div>
                <div class="section-body">
                    @if(session('notification') ?? false)
                        <div class="alert alert-success alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>&times;</span>
                                </button>
                                {{ session('notification') }}
                            </div>
                        </div>
                    @endif
                    @if(session('error_notification') ?? false)
                        <div class="alert alert-danger alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>&times;</span>
                                </button>
                                {{ session('error_notification') }}
                            </div>
                        </div>
                    @endif
                    <a href="{{ url(route('back.story-add')) }}" class="btn btn-info"
                       style="margin-bottom: 25px">Create
                        Stories</a>
                    <div class="clearfix"></div>
                    <table class="table table-hover" id="story-table">
                        <thead>
                        <tr>
                            <th scope="col" style="width: 5%">No</th>
                            <th scope="col">Title</th>
                            <th scope="col">Plots</th>
                            <th scope="col">Status</th>
                            <th scope="col" style="width: 30%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($stories as $index => $story)
                            <tr>
                                <th scope="row">{{ $index + 1 }}</th>
                                <td>{{ $story->getTitle() }}</td>
                                <td style="width: 5%">
                                    <a href="{{ url(route('back.plot-index', [$story->getId()])) }}"
                                       class="badge badge-primary">Plots</a>
                                </td>
                                <td style="width: 7%;">
                                    @if($story->isPublished())
                                        <a href="#"
                                           class="badge badge-primary">Published</a>
                                    @else
                                        <a href="#" class="badge badge-warning">Unpublished</a>
                                    @endif
                                </td>
                                <td style="width: 20%">
                                    <a class="btn btn-icon btn-info"
                                       href="{{ url(route('back.story-view', [$story->getId()])) }}">
                                        <i class="far fa-eye"></i> Lihat
                                    </a>
                                    <a class="btn btn-icon btn-warning"
                                       href="{{ url(route('back.story-edit', [$story->getId()]))  }}">
                                        <i class="far fa-edit"></i> Ubah
                                    </a>
                                    <a class="btn btn-icon btn-danger"
                                       href="{{ url(route('back.story-delete', [$story->getId()])) }}">
                                        <i class="fas fa-times"></i> Hapus
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">
                                    <div class="empty-state" data-height="400"
                                         style="height: 400px;">
                                        <div class="empty-state-icon">
                                            <i class="fas fa-question"></i>
                                        </div>
                                        <h2>Kami tidak dapat menemukan cerita apapun</h2>
                                        <p class="lead">
                                            Untuk menyingkirkan pesan ini, silahkan isi
                                            dengan
                                            setidaknya 1 cerita.
                                        </p>
                                        <a class="btn btn-primary mt-4"
                                           href={{ url(route('back.story-add')) }}>Buat
                                            Story</a>
                                    </div>
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection

@section('scripts')
    @if(count($stories))
        <script src="{{ url('/assets/modules/datatables/datatables.min.js') }}"></script>
        <script src="{{ url('/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ url('/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
        <script>
          $('#story').dataTable({
            'columnDefs': [
              {
                'sortable': false,
                'targets': [2],
              },
            ],
          });</script>
    @endif
@endsection