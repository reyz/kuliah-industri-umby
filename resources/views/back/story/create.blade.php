@extends('layouts.backend')

@section('title', 'Create New Story')
@section('stylesheet')
    <link rel="stylesheet"
          href="{{ url('/assets/modules/summernote/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/css/customs.css') }}">
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
        @include('layouts.back._partials.sidebar')
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <a href="{{ url(route('back.story-index')) }}" class="btn btn-info"
                       style="margin-right: 25px">
                        <i class="fas fa-chevron-left"></i>
                        Kembali
                    </a>
                    <h1>Create Story</h1>
                </div>
                <div class="section-body">
                    @if(!$errors->isEmpty())
                        <div class="alert alert-danger alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>&times;</span>
                                </button>
                                @foreach($errors->all() as $error)
                                    <i class="fa fa-info-circle"></i>{{ $error }}<br/>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <form method="POST" action="{{ url(route('back.story-add')) }}"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input class="form-control col-md-6 col-sm-12 {!! empty($errors->first('title')) ? '' : 'is-invalid' !!}"
                                           name="title" type="text"
                                           id="title"
                                           value="{{ request()->old('title') }}">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('title') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="custom-switch mt-2" for="published">
                                        <input type="checkbox"
                                               name="published"
                                               class="custom-switch-input" id="published">
                                        <span class="custom-switch-indicator"></span>
                                        <span class="custom-switch-description">Published</span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <button value="submit" class="btn btn-success">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection