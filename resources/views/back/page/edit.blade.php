@extends('layouts.backend')

@section('title')
    Ubah Halaman
@endsection

@section('stylesheet')
    <link rel="stylesheet"
          href="{{ url('/assets/modules/summernote/summernote-bs4.css') }}">
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
    @include('layouts.back._partials.sidebar')

    <!-- Main Content -->
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>Ubah Halaman</h1>
                </div>
                <div class="section-body">
                    @if(!$errors->isEmpty())
                        <div class="alert alert-danger alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>&times;</span>
                                </button>
                                @foreach($errors->all() as $error)
                                    <i class="fa fa-info-circle"></i>{{ $error }}<br/>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <div class="clearfix"></div>
                    <form method="POST">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="title">Judul</label>
                                    <input id="title"
                                           class="form-control {!! empty($errors->first('title')) ? '' : 'is-invalid' !!}}"
                                           name="title"
                                           type="text"
                                           value="{{ $page->getTitle() }}">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('title') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="content">Konten</label>
                                    <textarea id="content" class="summernote"
                                              name="description">
                                        {!! htmlspecialchars_decode($page->getContent()) !!}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success btn-icon icon-left"
                                            type="submit">
                                        <i class="fas fa-check"></i>Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection

@section('scripts')
    <script src="{{ url('/assets/modules/summernote/summernote-bs4.js') }}"></script>
    <script>
      $(document).ready(() => {
        $('.summernote').summernote({
          height: 300,
          toolbar: [
            ['style', ['stye']],
            ['font', ['bold', 'italic', 'underline']],
            ['fontname', ['fontame']],
            ['color', ['color']],
            ['height', ['height']],
            ['para', ['ul', 'ol', 'paragrahp']],
          ],
        });
      });
    </script>
@endsection
