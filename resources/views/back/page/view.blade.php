@extends('layouts.backend')

@section('title')
    Preview Halaman
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
    @include('layouts.back._partials.sidebar')
    <!-- Main Content -->
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <a href="{{ url(route('back.page-index')) }}" class="btn btn-info" style="margin-right: 25px">
                        <i class="fas fa-chevron-left"></i>
                        Kembali
                    </a>
                    <h1>Preview Halaman</h1>
                </div>
                <div class="section-body">
                    <div class="clearfix"></div>
                    <h1>{{ $page->getTitle() }}</h1>
                    <hr>
                    {!! $page->getContent() !!}
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection
