@extends('layouts.backend')

@section('title', 'Membuat Halaman Baru')
@section('stylesheet')
    <link rel="stylesheet"
          href="{{ url('/assets/modules/summernote/summernote-bs4.css') }}">
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
        @include('layouts.back._partials.sidebar')
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <a href="{{ url(route('back.page-index')) }}"
                       class="btn btn-info"
                       style="margin-right: 25px">
                        <i class="fas fa-chevron-left"></i>
                        Kembali
                    </a>
                    <h1>Menambahkan Halaman</h1>
                </div>
                <div class="section-body">
                    @if(!$errors->isEmpty())
                        <div class="alert alert-danger alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>&times;</span>
                                </button>
                                @foreach($errors->all() as $error)
                                    <i class="fa fa-info-circle"></i>{{ $error }}<br/>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <form method="POST" action="{{ url(route('back.page-add')) }}">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <div class="form-group">
                                    <label for="judul">Nama Halaman</label>
                                    <input id="judul"
                                           class="form-control col-md-6 col-sm-12 {!! empty($errors->first('title')) ? '' : 'is-invalid' !!}"
                                           name="title" type="text"
                                           value="{{ request()->old('title') }}">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('title') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="isi-content">Isi Halaman</label>
                                    <textarea class="summernote" id="isi-content"
                                              name="description">{{request()->old('description')}}</textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success" type="submit">Submit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection

@section('scripts')
    <script src="{{ url('/assets/modules/summernote/summernote-bs4.js') }}"></script>
    <script>
      $(document).ready(() => {
        $('.summernote').summernote({
          height: 300,
          toolbar: [
            ['style', ['stye']],
            ['font', ['bold', 'italic', 'underline']],
            ['fontname', ['fontame']],
            ['color', ['color']],
            ['height', ['height']],
            ['para', ['ul', 'ol', 'paragrahp']],
          ],
        });
      });
    </script>
@endsection