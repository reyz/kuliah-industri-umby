@extends('layouts.backend')

@section('title')
    Daftar Halaman
@endsection

@section('stylesheet')
    @if(count($pages))
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/datatables.min.css') }}">
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
    @endif
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
    @include('layouts.back._partials.sidebar')
    <!-- Main Content -->
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>Daftar Halaman</h1>
                </div>
                <div class="section-body">
                    <div class="clearfix"></div>
                    @if(session('system_notification') || session('system_notification') ?? false)
                        <div class="alert {{ session('system_notification') ? 'alert-primary' : 'alert-danger' }} alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>×</span>
                                </button>
                                {{ session('system_notification') }}
                                {{ session('error_notification') }}
                            </div>
                        </div>
                    @endif
                    <a href="{{ url(route('back.page-add')) }}" class="btn btn-info"
                       style="margin-bottom: 25px">Tambah Halaman</a>
                    <table class="table table-hover" id="data-table">
                        <thead>
                        <tr>
                            <th scope="col" style="width: 5%">ID</th>
                            <th scope="col" style="width: 70%">Judul</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($pages as $page)
                            <tr>
                                <th scope="row">{{ $page->getId() }}</th>
                                <td>{{ $page->getTitle() }}</td>
                                <td>
                                    <a class="btn btn-icon btn-info"
                                       href="{{ url(route('back.page-view', [$page->getId()])) }}">
                                        <i class="far fa-eye"></i> Lihat
                                    </a>
                                    <a class="btn btn-icon btn-warning"
                                       href="{{ url(route('back.page-edit', [$page->getId()])) }}">
                                        <i class="far fa-edit"></i> Ubah
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">
                                    <div class="empty-state" data-height="400"
                                         style="height: 400px;">
                                        <div class="empty-state-icon">
                                            <i class="fas fa-question"></i>
                                        </div>
                                        <h2>Tidak ada halaman!</h2>
                                        <p class="lead">
                                            Tidak ada halaman ditemukan.
                                        </p>
                                    </div>
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection

@section('scripts')
    @if(count($pages))
        <script src="{{ url('/assets/modules/datatables/datatables.js') }}"></script>
        <script src="{{ url('/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ url('/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
        <script>
          $('#data-table').dataTable();
        </script>
    @endif
@endsection
