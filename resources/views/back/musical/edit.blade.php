@extends('layouts.backend')

@section('title', 'Edit Musical')
@section('stylesheet')
    <link rel="stylesheet"
          href="{{ url('/assets/modules/summernote/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/css/customs.css') }}">
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
        @include('layouts.back._partials.sidebar')
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <a href="{{ url(route('back.musical-index')) }}"
                       class="btn btn-info"
                       style="margin-right: 25px">
                        <i class="fas fa-chevron-left"></i>
                        Kembali
                    </a>
                    <h1>Edit Musical Instruments - {{ $music->getName() }}</h1>
                </div>
                <div class="section-body">
                    @if(!$errors->isEmpty())
                        <div class="alert alert-danger alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>&times;</span>
                                </button>
                                @foreach($errors->all() as $error)
                                    <i class="fa fa-info-circle"></i>{{ $error }}<br/>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <form method="POST"
                          action="{{ url(route('back.musical-edit', [$music->getId()])) }}"
                          enctype="multipart/form-data">
                        <input type="hidden" name="id" value="{{ $music->getId() }}">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input class="form-control col-md-6 col-sm-12 {!! empty($errors->first('nama')) ? '' : 'is-invalid' !!}"
                                           name="nama" type="text"
                                           value="{{ $music->getName() }}">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('nama') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="history-content">Sejarah</label>
                                    <textarea class="summernote"
                                              id="history-content"
                                              name="sejarah">{{ htmlspecialchars_decode($music->getHistory()) }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group">
                                    <div class="section-title">Audio</div>
                                    <div class="audio-preview" id="audio-preview">
                                        <label for="audio-preview" id="audio-label">Choose
                                            File</label>
                                        <input accept="audio/*" id="audio-upload"
                                               name="audio" type="file"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="section-title">Gambar</div>
                                    <div class="image-preview" id="image-preview">
                                        <label for="image-upload" id="image-label">Choose
                                            File</label>
                                        <input accept="image/*" id="image-upload"
                                               name="picture" type="file">
                                    </div>

                                </div>
                                <button class="btn btn-success col-12"
                                        role="button">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection

@section('scripts')
    <script src="{{ url('/assets/modules/summernote/summernote-bs4.js') }}"></script>
    <script src="{{ url('/assets/modules/upload-preview/assets/js/jquery.uploadPreview.js') }}"></script>
    <script>
      $(document).ready(() => {
        $('.summernote').summernote({
          height: 300,
          toolbar: [
            ['style', ['stye']],
            ['font', ['bold', 'italic', 'underline']],
            ['fontname', ['fontame']],
            ['color', ['color']],
            ['height', ['height']],
            ['para', ['ul', 'ol', 'paragrahp']],
          ],
        });

        $.uploadPreview({
          input_field: '#image-upload',
          preview_box: '#image-preview',
          label_field: '#image-label',
          label_default: 'Choose File',
          label_selected: 'Change File',
          no_label: false,
          success_callback: null,
        });

        $.uploadPreview({
          input_field: '#audio-upload',
          label_field: '#audio-label',
          label_default: 'Choose File',
          label_selected: 'Change File',
          preview_box: '#audio-preview',
          no_label: false,
        });
      });
    </script>
@endsection