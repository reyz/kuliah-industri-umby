@extends('layouts.backend')

@section('title')
    Musical Instruments
@endsection

@section('stylesheet')
    @if(count($musical))
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/datatables.min.css') }}">
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
    @endif
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
        @include('layouts.back._partials.sidebar')
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>Musical Instruments</h1>
                </div>
                <div class="section-body">
                    @if(session('notification') ?? false)
                        <div class="alert alert-success alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>&times;</span>
                                </button>
                                {{ session('notification') }}
                            </div>
                        </div>
                    @endif
                    <a href="{{ url(route('back.musical-add')) }}" class="btn btn-info"
                       style="margin-bottom: 25px">Create
                        Musical</a>
                    <div class="clearfix"></div>
                    <table class="table table-hover" id="musical-table">
                        <thead>
                        <tr>
                            <th scope="col" style="width: 5%">No</th>
                            <th scope="col">Name</th>
                            <th scope="col" style="width: 25%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($musical as $index => $music)
                            <tr>
                                <th scope="row">{{ $index + 1 }}</th>
                                <td>{{ $music->getName() }}</td>
                                <td style="width: 20%">
                                    <a class="btn btn-icon btn-info"
                                       href="{{ url(route('back.musical-view', [$music->getId()])) }}">
                                        <i class="far fa-eye"></i> Lihat
                                    </a>
                                    <a class="btn btn-icon btn-warning"
                                       href="{{ url(route('back.musical-edit', [$music->getId()]))  }}">
                                        <i class="far fa-edit"></i> Ubah
                                    </a>
                                    <a class="btn btn-icon btn-danger"
                                       href="{{ url(route('back.musical-delete', [$music->getId()])) }}">
                                        <i class="fas fa-times"></i> Hapus
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">
                                    <div class="empty-state" data-height="400"
                                         style="height: 400px;">
                                        <div class="empty-state-icon">
                                            <i class="fas fa-question"></i>
                                        </div>
                                        <h2>Kami tidak dapat menemukan musik apapun</h2>
                                        <p class="lead">
                                            Untuk menyingkirkan pesan ini, silahkan isi
                                            dengan
                                            setidaknya 1 musik.
                                        </p>
                                        <a class="btn btn-primary mt-4"
                                           href={{ url(route('back.musical-add')) }}>Buat
                                            musik</a>
                                    </div>
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection

@section('scripts')
    @if(count($musical))
        <script src="{{ url('/assets/modules/datatables/datatables.min.js') }}"></script>
        <script src="{{ url('/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ url('/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
        <script>
          $('#musical-table').dataTable({
            'columnDefs': [
              {
                'sortable': false,
                'targets': [2],
              },
            ],
          });</script>
    @endif
@endsection