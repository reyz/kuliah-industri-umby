@extends('layouts.backend')

@section('title')
    View Musical Instruments
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
        @include('layouts.back._partials.sidebar')
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <a href="{{ url(route('back.musical-index')) }}"
                       class="btn btn-info"
                       style="margin-right: 25px">
                        <i class="fas fa-chevron-left"></i>
                        Kembali
                    </a>
                    <h1>Preview {{ $music->getName() }}</h1>
                </div>
                <div class="section-body">
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <h4>Nama Musical Instruments</h4>
                            <p>{{ $music->getName() }}</p>

                            <h4>Sejarah</h4>
                            <p>{!! $music->getHistory()  !!}</p>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <h4>Gambar</h4>
                            <img src="{{ url('/assets/uploads/'.$music->getPicture()) }}"
                                 alt="{{ $music->getName() }}">
                            <div class="clearfix"></div>
                            <br>
                            <h4>Audio</h4>
                            <audio controls>
                                <source src="{{ url('/assets/uploads/'.$music->getAudio()) }}"
                                        type="audio/wav">
                                Your browser does not support the audio element.
                            </audio>
                            <p></p>
                        </div>

                    </div>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection
