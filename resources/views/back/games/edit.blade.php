@extends('layouts.backend')

@section('title', 'Edit Permainan Tradisional')
@section('stylesheet')
    <link rel="stylesheet"
          href="{{ url('/assets/modules/summernote/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/css/customs.css') }}">
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
        @include('layouts.back._partials.sidebar')
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <a href="{{ url(route('back.game-index')) }}" class="btn btn-info"
                       style="margin-right: 25px">
                        <i class="fas fa-chevron-left"></i>
                        Kembali
                    </a>
                    <h1>Memperbaharui Permainan Tradisional</h1>
                </div>
                <div class="section-body">
                    @if(!$errors->isEmpty())
                        <div class="alert alert-danger alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>&times;</span>
                                </button>
                                @foreach($errors->all() as $error)
                                    <i class="fa fa-info-circle"></i>{{ $error }}<br/>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <form method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <div class="form-group">
                                    <label for="judul">Judul</label>
                                    <input id="judul"
                                           class="form-control col-md-6 col-sm-12 {!! empty($errors->first('title')) ? '' : 'is-invalid' !!}"
                                           name="title" type="text"
                                           value="{{ request()->old('title') ? request()->old('title') : $game->getTitle() }}">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('title') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="history-content">Sejarah</label>
                                    <textarea class="summernote" id="history-content"
                                              name="history">{{request()->old('history') ? request()->old('history') : htmlspecialchars_decode($game->getHistory())}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="philosophy">Filosofi</label>
                                    <textarea class="summernote" id="philosophy"
                                              name="philosophy">{{request()->old('philosophy') ? request()->old('philosophy') : htmlspecialchars_decode($game->getPhilosophy())}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="how-to-play">Cara Bermain</label>
                                    <textarea class="summernote" id="how-to-play"
                                              name="how-to-play">{{request()->old('how-to-play') ? request()->old('how-to-play') : htmlspecialchars_decode($game->getHowToPlay())}}</textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-success" type="submit">Submit
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-12">
                                <div class="form-group">
                                    <div class="section-title">Gambar</div>
                                    <div class="image-preview" id="image-preview"
                                         style="background-image: url({{ url('/assets/uploads/'.$game->getPicture()) }}); background-size: cover">
                                        <label for="image-upload" id="image-label">Choose
                                            File</label>
                                        <input accept="image/*" id="image-upload"
                                               name="image" type="file">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection

@section('scripts')
    <script src="{{ url('/assets/modules/summernote/summernote-bs4.js') }}"></script>
    <script src="{{ url('/assets/modules/upload-preview/assets/js/jquery.uploadPreview.js') }}"></script>
    <script>
      $(document).ready(() => {
        $('.summernote').summernote({
          height: 300,
          toolbar: [
            ['style', ['stye']],
            ['font', ['bold', 'italic', 'underline']],
            ['fontname', ['fontame']],
            ['color', ['color']],
            ['height', ['height']],
            ['para', ['ul', 'ol', 'paragrahp']],
          ],
        });
        $.uploadPreview({
          input_field: '#image-upload',
          preview_box: '#image-preview',
          label_field: '#image-label',
          label_default: 'Choose File',
          label_selected: 'Change File',
          no_label: false,
          success_callback: null,
        });
      });
    </script>
@endsection