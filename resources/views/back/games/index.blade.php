@extends('layouts.backend')

@section('title')
    Daftar Permainan Tradisional
@endsection

@section('stylesheet')
    @if(count($games))
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/datatables.min.css') }}">
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet"
              href="{{ url('/assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
    @endif
@endsection

@section('body')
    <div class="main-wrapper main-wrapper-1">
    @include('layouts.back._partials.sidebar')
    <!-- Main Content -->
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>Daftar Permainan Tradisional</h1>
                </div>
                <div class="section-body">
                    @if(session('notification') || session('error_notification') ?? false)
                        <div class="alert {{ session('notification') ? 'alert-primary' : 'alert-danger' }} alert-dismissible show fade">
                            <div class="alert-body">
                                <button class="close" data-dismiss="alert">
                                    <span>×</span>
                                </button>
                                {{ session('notification') }}
                                {{ session('error_notification') }}
                            </div>
                        </div>
                    @endif

                    <a href="{{ url(route('back.game-add')) }}" class="btn btn-info"
                       style="margin-bottom: 25px">Tambah Permainan</a>
                    <div class="clearfix"></div>
                    <table class="table table-hover" id="data-table">
                        <thead>
                        <tr>
                            <th scope="col" style="width: 5%">No</th>
                            <th scope="col" style="width: 60%">Judul</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($games))
                            <?php /** @var App\Entities\Games $game */ ?>
                            @foreach($games as $index => $game)
                                <tr>
                                    <th scope="row">{{ $index + 1 }}</th>
                                    <td>{{ $game->getTitle() }}</td>
                                    <td class="text-center">
                                        <a class="btn btn-icon btn-info "
                                           href="{{ url(route('back.game-view', [$game->getId()])) }}">
                                            <i class="far fa-eye"></i> Lihat
                                        </a>
                                        <a class="btn btn-icon btn-warning "
                                           href="{{ url(route('back.game-edit', [$game->getId()])) }}">
                                            <i class="far fa-edit"></i> Ubah
                                        </a>
                                        <a class="btn btn-icon btn-danger "
                                           href="{{ url(route('back.game-delete', [$game->getId()])) }}">
                                            <i class="fas fa-times"></i> Hapus
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">
                                    <div class="empty-state" data-height="400"
                                         style="height: 400px;">
                                        <div class="empty-state-icon">
                                            <i class="fas fa-question"></i>
                                        </div>
                                        <h2>No record found!</h2>
                                        <p class="lead">
                                            Tidak ada data permainan tradisional
                                            ditemukan.
                                        </p>
                                    </div>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection

@section('scripts')
    @if(count($games))
        <script src="{{ url('/assets/modules/datatables/datatables.js') }}"></script>
        <script src="{{ url('/assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ url('/assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
        <script>
          $('#data-table').dataTable({
            'columnDefs': [
              {
                'sortable': false,
                'targets': [2],
              },
            ],
          });
        </script>
    @endif
@endsection
