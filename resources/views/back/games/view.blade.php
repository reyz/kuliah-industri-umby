@extends('layouts.backend')

@section('title', 'Lihat Permainan')
@section('body')
    <div class="main-wrapper main-wrapper-1">
        @include('layouts.back._partials.sidebar')
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <a href="{{ url(route('back.game-index')) }}" class="btn btn-info"
                       style="margin-right: 25px">
                        <i class="fas fa-chevron-left"></i>
                        Kembali
                    </a>
                    <h1>Preview Permainan - {{ $game->getTitle() }}</h1>
                </div>
                <div class="section-body">
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12">
                            <div id="accordion">
                                <div class="accordion">
                                    <div class="accordion-header collapsed" role="button"
                                         data-toggle="collapse"
                                         data-target="#panel-body-picture"
                                         aria-expanded="false">
                                        <h4>Gambar</h4>
                                    </div>
                                    <div class="accordion-body collapse"
                                         id="panel-body-picture" data-parent="#accordion">
                                        <img src="{{ url('/assets/uploads/'.$game->getPicture()) }}"
                                             width="100%"/>
                                    </div>
                                </div>
                                <div class="accordion">
                                    <div class="accordion-header collapsed" role="button"
                                         data-toggle="collapse"
                                         data-target="#panel-body-sejarah"
                                         aria-expanded="false">
                                        <h4>Sejarah</h4>
                                    </div>
                                    <div class="accordion-body collapse"
                                         id="panel-body-sejarah" data-parent="#accordion">
                                        <p class="mb-0">{!! htmlspecialchars_decode($game->getHistory()) !!}</p>
                                    </div>
                                </div>
                                <div class="accordion">
                                    <div class="accordion-header collapsed" role="button"
                                         data-toggle="collapse"
                                         data-target="#panel-body-filosofi"
                                         aria-expanded="false">
                                        <h4>Filosofi</h4>
                                    </div>
                                    <div class="accordion-body collapse"
                                         id="panel-body-filosofi"
                                         data-parent="#accordion">
                                        <p class="mb-0">{!! htmlspecialchars_decode($game->getPhilosophy()) !!}</p>
                                    </div>
                                </div>
                                <div class="accordion">
                                    <div class="accordion-header collapsed" role="button"
                                         data-toggle="collapse"
                                         data-target="#panel-body-cara-bermain"
                                         aria-expanded="false">
                                        <h4>Cara Bermain</h4>
                                    </div>
                                    <div class="accordion-body collapse"
                                         id="panel-body-cara-bermain"
                                         data-parent="#accordion">
                                        <p class="mb-0">{!! htmlspecialchars_decode($game->getHowToPlay()) !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        @include('layouts.back._partials.footer')
    </div>
@endsection