<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>
        @yield('title')
    </title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ url('/assets/modules/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/modules/fontawesome/css/all.min.css') }}">
    @yield('stylesheet')

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ url('/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ url('/assets/css/components.css') }}">
</head>

<body>
<div id="app">
    @yield('body')
</div>

<!-- General JS Scripts -->
<script src="{{ url('/assets/modules/jquery.min.js') }}"></script>
<script src="{{ url('/assets/modules/popper.js') }}"></script>
<script src="{{ url('/assets/modules/tooltip.js') }}"></script>
<script src="{{ url('/assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ url('/assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
<script src="{{ url('/assets/modules/moment.min.js') }}"></script>
<script src="{{ url('/assets/js/stisla.js') }}"></script>

@yield('scripts')

<!-- Template JS File -->
<script src="{{ url('/assets/js/scripts.js') }}"></script>
<script src="{{ url('/assets/js/custom.js') }}"></script>

</body>
</html>
