<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no"
          name="viewport">
    <title>
        @yield('title')
    </title>

    <!-- General CSS Files -->
    <link href="{{ url('/assets/modules/bootstrap/css/bootstrap.css') }}"
          type="text/css" rel="stylesheet"/>
    <link rel="stylesheet"
          href="{{ url('/assets/modules/fontawesome/css/all.min.css') }}">

    <!-- Custom CSS Files -->
    <link href="{{ url('/assets/css/custom.css') }}" type="text/css" rel="stylesheet"/>
</head>
<body>

<div class="app d-flex align-items-center">
    <div class="container d-flex align-items-end">
        <div class="border-background">
            @if(Route::is('front.home-index'))
                <p class="text-welcome">Selamat Datang</p>
            @endif
            <div class="row" style="height: 100%">
                @yield('body-left')
                <div class="col-1"></div>
                @yield('body-right')
            </div>
        </div>
    </div>
</div>

<script src="{{ url('/assets/modules/jquery.min.js') }}"></script>
<script src="{{ url('/assets/modules/popper.js') }}"></script>
<script src="{{ url('/assets/modules/bootstrap/js/bootstrap.bundle.js') }}"></script>

@yield('scripts')

</body>
</html>
