<div class="navbar-bg"></div>

@include('layouts.back._partials.topbar');

<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="">Pojok Baca</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="javascript:void(0)">PBBY</a>
        </div>
        <ul class="sidebar-menu">
            <li class="{{ Route::is('back.page-*') ? 'active' : '' }}">
                <a class="nav-link"
                   href="{{ url(route('back.page-index')) }}">
                    <i class="fas fa-file"></i>
                    <span>Halaman</span>
                </a>
            </li>
            <li class="{{ Route::is('back.story-*') || Route::is('back.plot-*') ? 'active' : '' }}">
                <a class="nav-link"
                   href="{{ url(route('back.story-index')) }}">
                    <i class="fas fa-pencil-ruler"></i>
                    <span>Cerita Rakyat</span>
                </a>
            </li>
            <li class="{{ Route::is('back.game-*') ? 'active' : '' }}">
                <a class="nav-link"
                   href="{{ url(route('back.game-index')) }}">
                    <i class="fas fa-play"></i>
                    <span>Permainan Tradisional</span>
                </a>
            </li>
            <li class="{{ Route::is('back.song-*') ? 'active' : '' }}">
                <a class="nav-link"
                   href="{{ url(route('back.song-index')) }}">
                    <i class="fas fa-music"></i>
                    <span>Lagu Daerah</span>
                </a>
            </li>
            <hr>
            <li class="{{ Route::is('back.ranking-*') ? 'active' : '' }}">
                <a class="nav-link"
                   href="{{ url(route('back.ranking-index')) }}">
                    <i class="fas fa-trophy"></i>
                    <span>Ranking</span>
                </a>
            </li>
        </ul>
    </aside>
</div>
