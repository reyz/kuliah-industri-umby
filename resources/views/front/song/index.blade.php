@extends('layouts.frontend')
@section('title')
    Pojok Baca Budaya Yogyakarta | Musik
@endsection

@section('body-left')
    <div class="col-md-3 col-lg-3 col-sm-12 d-flex align-items-center justify-content-center">
        <img src="{{ url("/assets/img/semar.png") }}" class="img-fluid"
             alt="wayang"
             style="height: 30vw;"/>
    </div>
@endsection

@section('body-right')
    <div class="col-md-8 col-lg-8 col-sm-12 d-flex flex-column justify-content-around p-3">
        <div class="d-flex flex-column justify-content-around h-100">
            <div class="d-flex bg-warning flex-column align-items-center justify-content-center h-75 px-5">
                <p class="text-center text-notification">Selamat Datang Di Media
                    Pembelajaran
                    Interaktif Lagu
                    Daerah</p>
                <p class="text-center text-notification">Di Media Ini Kamu Akan
                    Mempelajari
                    Tentang Berbagai
                    Lagu Daerah</p>
            </div>
            <div class="d-flex justify-content-around px-5 "
                 style="padding: 0 10vw !important;">
                <a href="{{ url(route('front.home-index')) }}"
                   class="btn btn-lg btn-success">Kembali</a>
                <a href="{{ url(route('front.song-list')) }}"
                   class="btn btn-lg btn-success">Lanjut</a>
            </div>
        </div>
    </div>
@endsection