@extends('layouts.frontend')
@section('title')
    Pojok Baca Budaya Yogyakarta | Song List
@endsection

@section('body-left')
    <div class="col-md-9 col-lg-9 col-sm-12 d-flex align-items-center justify-content-center">
        <video class="w-100" controls autoplay>
            <source src="{{ url('/assets/uploads/songs/'.$songs->getVideo()) }}">
        </video>
    </div>
@endsection

@section('body-right')
    <div class="col-md-2 col-lg-2 col-sm-12 d-flex flex-column justify-content-around p-3">
        <div class="d-flex flex-column justify-content-between h-100">
            <div class="align-self-end px-5">
                <a href="{{ url(route('front.home-index')) }}"
                   class="btn btn-lg btn-success">Home</a>
            </div>
        </div>
    </div>
@endsection
