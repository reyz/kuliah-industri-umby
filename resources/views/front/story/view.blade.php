@extends('layouts.frontend')
@section('title')
    Pojok Baca Budaya Yogyakarta | Preview Story
@endsection
@section('body-left')
    <div class="col-12 d-flex flex-column justify-content-around p-3 h-100">
        <div class="d-flex flex-column h-100">
            <div class="align-self-end" style="z-index: 2">
                <a href="{{ url(route('front.home-index')) }}"
                   class="btn btn-lg btn-success">Home</a>
            </div>

            <div class="h-100 px-1 d-flex align-items-around justify-content-center"
                 style="margin-top: -6%">
                <div class="d-flex flex-column align-items-center justify-content-around w-100">
                    @if($story->result)
                        <h1>{{ $story->result->getPlot()->getPlotTitle() }}</h1>
                        <img src="{{ url('/assets/uploads/stories/'. $story->result->getFilename()) }}"
                             class="img-fluid img-story"
                             height="auto"
                        />
                        {!! htmlspecialchars_decode($story->result->getPlot()->getContent()) !!}
                        <div class="w-50">
                            <div class="d-flex justify-content-around">
                                <a href="{{ url(route('front.story-view', [
                                    $story->result->getPlot()->getStory()->getId(),
                                    $story->result->getPlot()->isPage() - 1
                                ]
                                )) }}"
                                   class="btn btn-success btn-lg btn-icon icon-left
                                   {{ $story->result->getPlot()->getPage() === 1 ? 'disabled' : '' }}">
                                    <i class="fas fa-chevron-circle-left fa-3x"></i>
                                </a>
                                <a href="{{ url(route('front.story-view', [
                                    $story->result->getPlot()->getStory()->getId(),
                                    $story->result->getPlot()->isPage() + 1
                                ]
                                )) }}"
                                   class="btn btn-success btn-lg btn-icon icon-left
                                    {{ $story->totalPage === $story->result->getPlot()->getPage() ? 'disabled' : ''}}
                                           ">
                                    <i class="fas fa-chevron-circle-right fa-3x"></i>
                                </a>
                            </div>
                        </div>
                    @else
                        <h1>Upps, Sepertinya masih belum ada Ceritanya.</h1>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection