@extends('layouts.frontend')
@section('Title')
    Pojok Baca Budaya Yogyakarta | Home
@endsection

@section('body-left')
    <div class="col-md-5 col-lg-5 col-sm-12 d-flex align-items-center justify-content-center">
        <img src="{{ url("/assets/img/semar.png") }}" class="img-fluid"
             alt="Wayang"
             style="height: 44vw;"/>
    </div>
@endsection

@section('body-right')
    <div class="col-md-6 col-lg-6 col-sm-12 d-flex flex-column justify-content-around p-3">
        <a href="{{ url(route('front.song-index')) }}"
           class="btn btn-lg btn-success p-lg-4 p-md-4 p-sm-3">LAGU DAERAH</a>
        <a href="{{ url(route('front.story-index')) }}"
           class="btn btn-lg btn-success p-lg-4 p-md-4 p-sm-3">DONGENG</a>
        <a href="{{ url(route('front.game-index')) }}"
           class="btn btn-lg btn-success p-lg-4 p-md-4 p-sm-3">PERMAINAN TRADISIONAL</a>
        @foreach($pages as $page)
            <a href="{{ url(route('front.page-view', strtolower($page->getSlug()))) }}"
               class="btn btn-lg btn-success p-lg-4 p-md-4 p-sm-3">{{ strtoupper($page->getTitle()) }}</a>
        @endforeach
    </div>
@endsection