@extends('layouts.frontend')
@section('title')
    Permainan Tradisional | Pojok Baca Budaya Yogyakarta
@endsection

@section('body-left')
    <style>
        .app {
            height: 100% !important;
            min-height: 100vh;
        }

        .app .container {
            min-height: 95vh;
            margin-top: 2% !important;
            margin-bottom: 2% !important;
        }

        .app .border-background {
            min-height: 92vh;
        }
    </style>
    <div class="col-12 d-flex flex-column justify-content-around p-3 h-100">
        <div class="d-flex flex-column h-100">
            <div class="d-flex justify-content-between px-5 mb-5">
                <a class="btn btn-lg btn-success btn-icon icon-left"
                   href="{{ url(route('front.game-list')) }}">
                    <i class="fas fa-chevron-circle-left"></i> Kembali</a>
                <a href="{{ url(route('front.home-index')) }}"
                   class="btn btn-lg btn-success btn-icon icon-left">
                    <i class="fas fa-home"></i> Home</a>
            </div>
            <img src="{{ url('/assets/uploads/games/'.$game->getPicture()) }}"
                 class="img-fluid align-self-center mb-3 img-game"
                 alt="Gambar {{ $game->getTitle() }}"/>
            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <h2 class="text-center">Filosofi</h2>
                    {!! htmlspecialchars_decode($game->getPhilosophy()) !!}
                </div>
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <h2 class="text-center">Cara Bermain</h2>
                    {!! htmlspecialchars_decode($game->getHowToPlay()) !!}
                </div>
            </div>
        </div>
    </div>
@endsection