@extends('layouts.frontend')
@section('title')
    Daftar Permain Tradisional | Pojok Baca Budaya Yogyakarta
@endsection

@section('body-left')
    <div class="col-md-3 col-lg-3 col-sm-12 d-flex align-items-center justify-content-center">
        <img src="{{ url("/assets/img/semar.png") }}" class="img-fluid"
             alt="wayang"
             style="height: 30vw;"/>
    </div>
    <p class="position-absolute bg-warning text-perintah px-3">Silahkan Pilih Permainan
        Tradisonal Yang Ingin Di Baca</p>
@endsection

@section('body-right')
    <div class="col-md-8 col-lg-8 col-sm-12 d-flex flex-column justify-content-around p-3">
        <div class="d-flex flex-column justify-content-between h-100">
            <div class="align-self-end px-5 ">
                <a href="{{ url(route('front.home-index')) }}"
                   class="btn btn-lg btn-success btn-icon icon-left">
                    <i class="fas fa-home"></i> Home</a>
            </div>
            <div class="h-75 px-1">
                <div class="d-flex justify-content-around flex-wrap">
                @forelse($games as $game)
                        <a href="{{ url(route('front.game-view', $game->getId())) }}"
                           class="btn btn-md btn-danger btn-list">{{ $game->getTitle() }}</a>
                    @empty
                        <div class="d-flex align-items-center h-50">
                            <h1>Upps, Sepertinya masih belum ada Dongeng apa pun.</h1>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@endsection