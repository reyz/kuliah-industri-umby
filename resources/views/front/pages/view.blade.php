@extends('layouts.frontend')
@section('title')
    {{ $page->getTitle() }}| Pojok Baca Budaya Yogyakarta
@endsection

@section('body-left')
    <div class="col-12 d-flex flex-column justify-content-around p-3 h-100">
        <div class="d-flex flex-column h-100">
            <div class="align-self-end">
                <a href="{{ url(route('front.home-index')) }}"
                   class="btn btn-lg btn-success btn-icon icon-left">
                    <i class="fas fa-home"></i> Home</a>
            </div>

            <div class="h-50 px-1 d-flex align-items-around justify-content-center mt-3">
                <div class="d-flex flex-column align-items-center justify-content-around bg-warning border-background"
                     style="border-color: #fdd356;">
                    <h1 style="margin-top: 25px">{{ ucwords($page->getTitle()) }}</h1>
                    <div style="padding: 25px">
                        {!! htmlspecialchars_decode($page->getContent()) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection